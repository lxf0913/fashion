package edu.fashion.dao.order;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import edu.fashion.model.Order;
import edu.fashion.model.PageModel;
import edu.fashion.util.MySqlUtil;
import edu.fashion.util.SqlToString;
/**
 * 用户订单查询Dao命令  按user_id查询
 * @author 李政奇   
 *
 */  
public class SelectOrderDaoImpl { 
	public PageModel<Order> selectODI(String orderUserId,String page){
		if (page==null) {
			page="1";
		}
		String sqlOld ="SELECT * from `order` WHERE order_user_id =? and order_status='1' ORDER BY order_date DESC";
		String pageSize="8"; 
		PageModel<Order> pageAll=new PageModel<Order>(sqlOld, page, pageSize);
		String sqlNew=pageAll.toMysqlSql();
		try {
			List<HashMap<String, Object>> sqlList=MySqlUtil.selectSql(sqlNew, orderUserId);
			List<Order> orderList=new ArrayList<Order>();
			if(sqlList.size()>0) {
				Order order=null;
				for(int i=0;i<sqlList.size();i++) {
					HashMap<String, Object> map=sqlList.get(i);
					order=new Order();
					order.setOrderAddress(SqlToString.sqlToString(map.get("order_address")));
					order.setOrderComment(SqlToString.sqlToString(map.get("order_comment")));
					order.setOrderDate(SqlToString.sqlToString(map.get("order_date")));
					order.setOrderId(SqlToString.sqlToString(map.get("order_id")));
					order.setOrderIsdeliver(SqlToString.sqlToString(map.get("order_isdeliver")));
					order.setOrderIspay(SqlToString.sqlToString(map.get("order_ispay")));
					order.setOrderPrice(SqlToString.sqlToString(map.get("order_price")));
					order.setOrderStatus(SqlToString.sqlToString(map.get("order_status")));
					order.setOrderUserId(SqlToString.sqlToString(map.get("order_user_id")));
					orderList.add(order);
				}
			}
			pageAll.setList(orderList);
			String sqlCount=pageAll.toCountSql();
			List<HashMap<String,Object>> countList=MySqlUtil.selectSql(sqlCount,orderUserId);
			HashMap<String, Object> map=countList.get(0);
			String count=SqlToString.sqlToString(map.get("count"));
			pageAll.setTotal(Integer.valueOf(count));
			pageAll.setTotalPage(pageAll.getTotalPage());
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}finally {	
			MySqlUtil.close();
		}	
		return pageAll;
	}
}
