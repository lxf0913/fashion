package edu.fashion.dao.order;
/**
 * 根据订单ID 
 将订单的状态设置为0
 * 李政奇
 */
import edu.fashion.util.MySqlUtil;

public class UpdataStatusOrderDaoImpl {
	 public int statusOrder (String orderId) {
		 String sql="UPDATE  `order` SET order_status = '0' WHERE order_id=? ";
		 int i=MySqlUtil.updateSql(sql, orderId);
		 MySqlUtil.close();
		 return i;
	 }
}
