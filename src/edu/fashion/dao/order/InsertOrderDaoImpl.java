package edu.fashion.dao.order;

import edu.fashion.util.MySqlUtil;

public class InsertOrderDaoImpl {
	public int insertOrder(String orderid,String userid,String address,String price) {
		String sql="insert into `order` (order_id,order_user_id,order_address,order_price) VALUES(?,?,?,?)";
		int i=0;
		try {
			i =MySqlUtil.updateSql(sql,orderid,userid,address,price);
		} catch (Exception e) {
			e.printStackTrace();	
		}finally {
			MySqlUtil.close();
		}
		return i;
	}
}
