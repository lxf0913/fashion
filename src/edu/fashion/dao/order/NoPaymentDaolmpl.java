package edu.fashion.dao.order;


import java.util.HashMap;
import java.util.List;

import edu.fashion.util.MySqlUtil;
import edu.fashion.util.SqlToString;

public class NoPaymentDaolmpl {
	public int selectnopay(String orderUserId) {
		String sql="SELECT count(*) as b FROM `order` WHERE order_user_id=? AND order_ispay='0' AND order_status='1' ;";
		 List<HashMap<String,Object>> sqlList=MySqlUtil.selectSql(sql, orderUserId);
		 HashMap<String, Object> map=sqlList.get(0);
		 int i=new Integer(SqlToString.sqlToString(map.get("b")));
		 MySqlUtil.close();
		
		return i;
	}
}
