package edu.fashion.dao.order;
	/**
	 * 根据用户id
	 * 更新支付状态 设置为 1
	 * @author 李政奇
	 *
	 */

import edu.fashion.util.MySqlUtil;

public class UpdataPayOrderDaoImpl {
	 public int payOrder (String orderId) {
		 String sql="UPDATE  `order` SET order_ispay = '1' WHERE order_id=? ";
		int i= MySqlUtil.updateSql(sql, orderId);
		 MySqlUtil.close();
		 return i;
	 }
}
