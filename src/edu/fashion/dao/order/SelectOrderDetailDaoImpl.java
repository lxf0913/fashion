package edu.fashion.dao.order;
/**
 * 李政奇
 * 查询订单详情
 */
import java.util.HashMap;
import java.util.List;

import edu.fashion.model.Order;
import edu.fashion.util.MySqlUtil;
import edu.fashion.util.SqlToString;

public class SelectOrderDetailDaoImpl {
	public Order orderDatail (String orderUserId,String orderId) {
		String sql="SELECT * FROM `order` WHERE order_user_id = ? AND order_id = ? ";
		 List<HashMap<String,Object>> sqlList=MySqlUtil.selectSql(sql, orderUserId,orderId);
		 Order order=null;
		try {
			if (sqlList!=null) {
				HashMap<String, Object> map=sqlList.get(0);
				order=new Order();
				order.setOrderAddress(SqlToString.sqlToString(map.get("order_address")));
				order.setOrderComment(SqlToString.sqlToString(map.get("order_comment")));
				order.setOrderDate(SqlToString.sqlToString(map.get("order_date")));
				order.setOrderId(SqlToString.sqlToString(map.get("order_id")));
				order.setOrderIsdeliver(SqlToString.sqlToString(map.get("order_isdeliver")));
				order.setOrderIspay(SqlToString.sqlToString(map.get("order_ispay")));
				order.setOrderPrice(SqlToString.sqlToString(map.get("order_price")));
				order.setOrderStatus(SqlToString.sqlToString(map.get("order_status")));
				order.setOrderUserId(SqlToString.sqlToString(map.get("order_user_id")));
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}finally {
			MySqlUtil.close();
		}
		return order;
	}
}
