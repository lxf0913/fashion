package edu.fashion.dao.collection;

import edu.fashion.util.MySqlUtil;


/*
 * lxf
 */
public class InsertCollectionDaoImpl {
	public int insertcollection(String userid,String productid) {
		String sql = "INSERT INTO collection (collection_user_id,collection_product_id) VALUES (?,?)";
		int i = MySqlUtil.updateSql(sql, userid, productid);
		MySqlUtil.close();
		return i;
	}
}
