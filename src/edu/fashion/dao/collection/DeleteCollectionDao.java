package edu.fashion.dao.collection;

import edu.fashion.util.MySqlUtil;

public class DeleteCollectionDao {
	public int deletecollection(String userid) {
		String sql = "delete from collection where collection_user_id=?";
		int i = MySqlUtil.updateSql(sql, userid);
		MySqlUtil.close();
		return i;
	}
}
