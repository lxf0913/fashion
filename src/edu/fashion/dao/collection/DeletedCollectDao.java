package edu.fashion.dao.collection;

import edu.fashion.util.MySqlUtil;

public class DeletedCollectDao {
	public int deletedcollection(String collectionid) {
		String sql = "delete from collection where collection_id=?";
		int i = MySqlUtil.updateSql(sql, collectionid);
		MySqlUtil.close();
		return i;
	}
}
