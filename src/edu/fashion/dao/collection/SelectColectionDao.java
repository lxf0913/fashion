package edu.fashion.dao.collection;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import edu.fashion.util.MySqlUtil;
import edu.fashion.util.SqlToString;

public class SelectColectionDao {
	public List<HashMap<String, Object>> select(String userid) {
		List<HashMap<String, Object>> colections = new ArrayList<HashMap<String, Object>>();
		String sql = "select * from collection A left join product B on A.collection_product_id=B.product_id where  A.collection_user_id=? ";
		try {
			List<HashMap<String, Object>> selectlist = MySqlUtil.selectSql(sql, userid);
			if (selectlist.size() > 0) {
				for (int i = 0; i < selectlist.size(); i++) {
					HashMap<String, Object> map =new  HashMap<String, Object>();
					map.put("colectionid", SqlToString.sqlToString(selectlist.get(i).get("collection_id")));
					map.put("time", SqlToString.sqlToString(selectlist.get(i).get("collection_time")));
					map.put("productid", SqlToString.sqlToString(selectlist.get(i).get("collection_product_id")));
					colections.add(map);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			MySqlUtil.close();
		}
		return colections;
	}
}
