package edu.fashion.dao.product;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import edu.fashion.model.PageModel;
import edu.fashion.model.Product;
import edu.fashion.util.MySqlUtil;
import edu.fashion.util.SqlToString;
/* 大类别  查询 销量商品排序 
 * 李政奇
 */
public class SelectSalesClassDaoImpl {
	public PageModel<Product> selectSalesClass(String className,String page,String pageSize) {
//		
//		如果默认是空值则置为第一页
		if (page==null) {
			page="1";
		}
//		不需要分页时的sql语句
		String sqlOld="SELECT p.*,category_name,class_name from product p LEFT JOIN category on product_category_id=category_id LEFT JOIN class on product_class_id=class_id where class_name=? and product_status='1' ORDER BY product_sales DESC ";
//		分页模型 构造函数 
		PageModel<Product> pageSelectSalesClass=new PageModel<Product>(sqlOld, page, pageSize);
//		追加分页sql语句 这里的sql语句执行完就是 页面上需要显示的数目
		String sqlNew= pageSelectSalesClass.toMysqlSql();
		try {
//			需要传参 参数与旧SQL语句一致
			List<HashMap<String, Object>> list =MySqlUtil.selectSql(sqlNew,className);
//			productList用来存储sqlNow返回的Product
			List<Product> productList=null;
			if(list.size()>0) {
				productList=new ArrayList<Product>();
				for(int i=0;i<list.size();i++) {
					Product product=new Product();
					product.setProductId(SqlToString.sqlToString(list.get(i).get("product_id")));
					product.setProductName(SqlToString.sqlToString(list.get(i).get("product_name")));
					product.setProductPrice(SqlToString.sqlToString(list.get(i).get("product_price")));
					product.setProductCount(SqlToString.sqlToString(list.get(i).get("product_count")));
					product.setProductStastus(SqlToString.sqlToString(list.get(i).get("product_status")));
					product.setProductSales(SqlToString.sqlToString(list.get(i).get("product_sales")));
					product.setProductDate(SqlToString.sqlToString(list.get(i).get("product_date")));
					product.setProductCategory(SqlToString.sqlToString(list.get(i).get("category_name")));
					product.setProductClassId(SqlToString.sqlToString(list.get(i).get("class_name")));
					product.setProductBrand(SqlToString.sqlToString(list.get(i).get("product_brand_id")));
					product.setProductPhoto(SqlToString.sqlToString(list.get(i).get("product_photo")));
					productList.add(product);
				}	
			}
			pageSelectSalesClass.setList(productList);
//			这里不要忘记传参
			List<HashMap<String,Object>> countlist =MySqlUtil.selectSql(pageSelectSalesClass.toCountSql(),className);
			HashMap<String, Object> map=countlist.get(0);
			String count=SqlToString.sqlToString(map.get("count"));
			pageSelectSalesClass.setTotal(Integer.valueOf(count));
			pageSelectSalesClass.setTotalPage(pageSelectSalesClass.getTotalPage());
		} catch (Exception e) {
			e.printStackTrace();	
		}finally {
			MySqlUtil.close();
		}
		return pageSelectSalesClass;
	}
}
