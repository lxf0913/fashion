package edu.fashion.dao.product;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import edu.fashion.model.PageModel;
import edu.fashion.model.Product;
import edu.fashion.util.MySqlUtil;
import edu.fashion.util.SqlToString;
/*
 * 李政奇
 * 小类别
 * 价格升序
 */
public class SelectCategoryAscDaoLmpl {
	public PageModel<Product> selectCategoryAsc(String categoryName,String page,String pageSize) {
		if (page==null) {
			page="1";
		}
		String sqlOld="SELECT p.*,category_name,class_name from product p LEFT JOIN category on product_category_id=category_id LEFT JOIN class on product_class_id=class_id where category_name=?and product_status='1' ORDER BY product_price";
		PageModel<Product> pagePriceCategory =new PageModel<Product>(sqlOld,page,pageSize);
		String	sqlNew=pagePriceCategory.toMysqlSql();
		try {
			List<Product> products=null;
			List<HashMap<String, Object>> list =MySqlUtil.selectSql(sqlNew,categoryName);
			if(list.size()>0) {
				products=new ArrayList<Product>();
				for(int i=0;i<list.size();i++) {
					Product product=new Product();
					product.setProductId(SqlToString.sqlToString(list.get(i).get("product_id")));
					product.setProductName(SqlToString.sqlToString(list.get(i).get("product_name")));
					product.setProductPrice(SqlToString.sqlToString(list.get(i).get("product_price")));
					product.setProductCount(SqlToString.sqlToString(list.get(i).get("product_count")));
					product.setProductStastus(SqlToString.sqlToString(list.get(i).get("product_status")));
					product.setProductSales(SqlToString.sqlToString(list.get(i).get("product_sales")));
					product.setProductDate(SqlToString.sqlToString(list.get(i).get("product_date")));
					product.setProductCategory(SqlToString.sqlToString(list.get(i).get("category_name")));
					product.setProductClassId(SqlToString.sqlToString(list.get(i).get("class_name")));
					product.setProductBrand(SqlToString.sqlToString(list.get(i).get("product_brand_id")));
					product.setProductPhoto(SqlToString.sqlToString(list.get(i).get("product_photo")));
					products.add(product);
				}	
			}
			pagePriceCategory.setList(products);
			String sqlTotal=pagePriceCategory.toCountSql();
			List<HashMap<String,Object>> countList=MySqlUtil.selectSql(sqlTotal,categoryName);
			HashMap<String, Object> map=countList.get(0);
			String count=SqlToString.sqlToString(map.get("count"));
			pagePriceCategory.setTotal(Integer.valueOf(count));
			pagePriceCategory.setTotalPage(pagePriceCategory.getTotalPage());
		} catch (Exception e) {
			e.printStackTrace();	
		}finally {
			MySqlUtil.close();
		}
		return pagePriceCategory;
	}
}
