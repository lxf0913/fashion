package edu.fashion.dao.product;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import edu.fashion.util.MySqlUtil;
import edu.fashion.util.SqlToString;

public class SelectCC {
	public HashMap<String, List<String>> selectClassC() {
		String sql="SELECT * from class";
		HashMap<String, List<String>> h=new HashMap<String, List<String>>();
		try {
			List<HashMap<String, Object>> list =MySqlUtil.selectSql(sql);
			List<String> clas=new ArrayList<String>();
			if(list.size()>0) {
				for(int i=0;i<list.size();i++) {
					clas.add(SqlToString.sqlToString(list.get(i).get("class_name")));
				}
			}
			h.put("class", clas);
			String sql1="SELECT * from category";
			List<HashMap<String, Object>> list1 =MySqlUtil.selectSql(sql1);
			List<String> cate=new ArrayList<String>();
			if(list1.size()>0) {
				for(int i=0;i<list1.size();i++) {
					cate.add(SqlToString.sqlToString(list1.get(i).get("category_name")));
				}
			}
			h.put("category", cate);
		} catch (Exception e) {
			e.printStackTrace();	
		}finally {
			MySqlUtil.close();
		}
		return h;
	}

}
