package edu.fashion.dao.product;

import java.util.HashMap;
import java.util.List;
import edu.fashion.util.MySqlUtil;
import edu.fashion.util.SqlToString;
/*
 * lxf
 */

public class SelectPhotoDaoImpl {
	public String[] selectPhoto(String Productid) {
		String sql="SELECT * from photo where photo_product_id=?";
		String[] photos=null;
		try {
			List<HashMap<String, Object>> list =MySqlUtil.selectSql(sql,Productid);
			if(list.size()>0) {
				photos=new String[list.size()];
				for(int i=0;i<list.size();i++) {
					photos[i]=SqlToString.sqlToString(list.get(i).get("photo_name"));
				}	
			}
		} catch (Exception e) {
			e.printStackTrace();	
		}finally {
			MySqlUtil.close();
		}
		return photos;
	}
}
