package edu.fashion.dao.product;

import java.util.HashMap;
import java.util.List;

import edu.fashion.model.Product;
import edu.fashion.util.MySqlUtil;
import edu.fashion.util.SqlToString;

public class SelectOrderIdProductDaoImpl {
	public Product orderIdProduct (String orderId) {
		String sql="SELECT * FROM (SELECT * FROM `order` o LEFT JOIN shopcar s ON o.order_id = s.shopcar_order_id) os LEFT JOIN product p ON os.shopcar_product_id =p.product_id WHERE order_id=? AND product_status='1' ";
		List<HashMap<String,Object>> sqlList=MySqlUtil.selectSql(sql, orderId);
		Product product=null;
		try {
			if(sqlList.size()>0) {
				HashMap<String, Object> map=sqlList.get(0);
				product=new Product();
				product.setProductId(SqlToString.sqlToString(map.get("product_id")));
				product.setProductName(SqlToString.sqlToString(map.get("product_name")));
				product.setProductPrice(SqlToString.sqlToString(map.get("product_price")));
				product.setProductCount(SqlToString.sqlToString(map.get("product_count")));
				product.setProductStastus(SqlToString.sqlToString(map.get("product_status")));
				product.setProductSales(SqlToString.sqlToString(map.get("product_sales")));
				product.setProductDate(SqlToString.sqlToString(map.get("product_date")));
				product.setProductCategory(SqlToString.sqlToString(map.get("category_name")));
				product.setProductClassId(SqlToString.sqlToString(map.get("class_name")));
				product.setProductBrand(SqlToString.sqlToString(map.get("product_brand_id")));
				product.setProductPhoto(SqlToString.sqlToString(map.get("product_photo")));
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}finally {
			MySqlUtil.close();
		}
		return product;
	}
}
