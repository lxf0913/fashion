package edu.fashion.dao.product;

import java.util.HashMap;
import java.util.List;

import edu.fashion.model.Product;
import edu.fashion.util.MySqlUtil;
import edu.fashion.util.SqlToString;

/**
 * @author 超超
 * 商品详情查询（根据商品ID查询单个商品）
 *
 */
public class SelectProductDaoImpl {
	public Product selectpdi(String productid) {
		String sql="SELECT p.*,category_name,class_name from product p LEFT JOIN category on product_category_id=category_id LEFT JOIN class on product_class_id=class_id where product_id=?";
		Product product=null;
		try {
			List<HashMap<String, Object>> list =MySqlUtil.selectSql(sql,productid);	
			if(list.size()>0) {
					product=new Product();
					product.setProductId(SqlToString.sqlToString(list.get(0).get("product_id")));
					product.setProductName(SqlToString.sqlToString(list.get(0).get("product_name")));
					product.setProductPrice(SqlToString.sqlToString(list.get(0).get("product_price")));
					product.setProductCount(SqlToString.sqlToString(list.get(0).get("product_count")));
					product.setProductStastus(SqlToString.sqlToString(list.get(0).get("product_status")));
					product.setProductSales(SqlToString.sqlToString(list.get(0).get("product_sales")));
					product.setProductDate(SqlToString.sqlToString(list.get(0).get("product_date")));
					product.setProductCategory(SqlToString.sqlToString(list.get(0).get("category_name")));
					product.setProductClassId(SqlToString.sqlToString(list.get(0).get("class_name")));
					product.setProductBrand(SqlToString.sqlToString(list.get(0).get("product_brand_id")));
					product.setProductPhoto(SqlToString.sqlToString(list.get(0).get("product_photo")));
			}
		} catch (Exception e) {
			e.printStackTrace();	
		}finally {
			MySqlUtil.close();
		}
		return product;
	}
}
