package edu.fashion.dao.product;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import edu.fashion.model.PageModel;
import edu.fashion.model.Product;
import edu.fashion.util.MySqlUtil;
import edu.fashion.util.SqlToString;

/**
 * 按照销量 查询全部商品升序
 * @author 李政奇
 *
 */
public class SelectSalesAllProductAscDaoImpl {
	public PageModel<Product> salesAllProductAsc(String page,String pageSize) {
		if (page==null) {
			page="1";
		}
		String sqlOld="SELECT p.*,category_name,class_name from product p LEFT JOIN category on product_category_id=category_id LEFT JOIN class on product_class_id=class_id where product_status='1' ORDER BY p.product_sales";
		PageModel<Product> pageSalesAllAsc=new PageModel<Product>(sqlOld,page,pageSize);
		String sqlNew=pageSalesAllAsc.toMysqlSql();
		List<Product> productlist=null;
		try {
			List<HashMap<String, Object>> sqlList=MySqlUtil.selectSql(sqlNew);
			if(sqlList.size()>0) {
				productlist=new ArrayList<Product>();
				for(int i=0;i<sqlList.size();i++) {
					Product product=new Product();
					product.setProductId(SqlToString.sqlToString(sqlList.get(i).get("product_id")));
					product.setProductName(SqlToString.sqlToString(sqlList.get(i).get("product_name")));
					product.setProductPrice(SqlToString.sqlToString(sqlList.get(i).get("product_price")));
					product.setProductCount(SqlToString.sqlToString(sqlList.get(i).get("product_count")));
					product.setProductStastus(SqlToString.sqlToString(sqlList.get(i).get("product_status")));
					product.setProductSales(SqlToString.sqlToString(sqlList.get(i).get("product_sales")));
					product.setProductDate(SqlToString.sqlToString(sqlList.get(i).get("product_date")));
					product.setProductCategory(SqlToString.sqlToString(sqlList.get(i).get("category_name")));
					product.setProductClassId(SqlToString.sqlToString(sqlList.get(i).get("class_name")));
					product.setProductBrand(SqlToString.sqlToString(sqlList.get(i).get("product_brand_id")));
					product.setProductPhoto(SqlToString.sqlToString(sqlList.get(i).get("product_photo")));
					productlist.add(product);
				}	
			}
			pageSalesAllAsc.setList(productlist);
			String sqlCount=pageSalesAllAsc.toCountSql();
			List<HashMap<String,Object>> countList=MySqlUtil.selectSql(sqlCount);
			HashMap<String, Object> map=countList.get(0);
			String count=SqlToString.sqlToString(map.get("count"));
			pageSalesAllAsc.setTotal(Integer.valueOf(count));
			pageSalesAllAsc.setTotalPage(pageSalesAllAsc.getTotalPage());
		} catch (Exception e) {
			e.printStackTrace();	
		}finally {
			MySqlUtil.close();
		}
		return pageSalesAllAsc;
	}
}
