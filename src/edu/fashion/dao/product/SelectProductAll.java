package edu.fashion.dao.product;

/**
 * 查询所有商品 不排序
 */
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import edu.fashion.model.PageModel;
import edu.fashion.model.Product;
import edu.fashion.util.MySqlUtil;
import edu.fashion.util.SqlToString;

public class SelectProductAll {
	public PageModel<Product> selectAllProduct(String page,String pageSize) {
		if (page==null) {
			page="1";
		}
		String sqlOld="SELECT p.*,category_name,class_name from product p LEFT JOIN category on product_category_id=category_id LEFT JOIN class on product_class_id=class_id where product_status='1'";
		PageModel<Product> pageAll=new PageModel<Product>(sqlOld, page, pageSize);
		String sqlNew=pageAll.toMysqlSql();
		try {
			List<HashMap<String, Object>> list =MySqlUtil.selectSql(sqlNew);
			List<Product> products=null;
			if(list.size()>0) {
				products=new ArrayList<Product>();
				for(int i=0;i<list.size();i++) {
					Product product=new Product();
					product.setProductId(SqlToString.sqlToString(list.get(i).get("product_id")));
					product.setProductName(SqlToString.sqlToString(list.get(i).get("product_name")));
					product.setProductPrice(SqlToString.sqlToString(list.get(i).get("product_price")));
					product.setProductCount(SqlToString.sqlToString(list.get(i).get("product_count")));
					product.setProductStastus(SqlToString.sqlToString(list.get(i).get("product_status")));
					product.setProductSales(SqlToString.sqlToString(list.get(i).get("product_sales")));
					product.setProductDate(SqlToString.sqlToString(list.get(i).get("product_date")));
					product.setProductCategory(SqlToString.sqlToString(list.get(i).get("category_name")));
					product.setProductClassId(SqlToString.sqlToString(list.get(i).get("class_name")));
					product.setProductBrand(SqlToString.sqlToString(list.get(i).get("product_brand_id")));
					product.setProductPhoto(SqlToString.sqlToString(list.get(i).get("product_photo")));
					products.add(product);
				}	
			}
			pageAll.setList(products);
			String sqlCount=pageAll.toCountSql();
			List<HashMap<String,Object>> countList=MySqlUtil.selectSql(sqlCount);
			HashMap<String, Object> map=countList.get(0);
			String count=SqlToString.sqlToString(map.get("count"));
			pageAll.setTotal(Integer.valueOf(count));
			pageAll.setTotalPage(pageAll.getTotalPage());
		} catch (Exception e) {
			e.printStackTrace();	
		}finally {
			MySqlUtil.close();
		}
		return pageAll;
	}
}
