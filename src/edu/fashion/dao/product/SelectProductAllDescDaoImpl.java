package edu.fashion.dao.product;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import edu.fashion.model.PageModel;
import edu.fashion.model.Product;
import edu.fashion.util.MySqlUtil;
import edu.fashion.util.SqlToString;
/*查询所有商品 按价格降序
 * 李政奇
 */
public class SelectProductAllDescDaoImpl {
	public PageModel<Product> selectPADIdesc(String page,String pageSize) {
		if (page==null) {
			page="1";
		}
		String sqlOld="SELECT p.*,category_name,class_name from product p LEFT JOIN category on product_category_id=category_id LEFT JOIN class on product_class_id=class_id where product_status='1' ORDER BY product_price desc";
		PageModel<Product> pagePriceAll=new PageModel<Product>(sqlOld, page, pageSize);
		String sqlNew=pagePriceAll.toMysqlSql();
		try {
			List<HashMap<String, Object>> sqlList=MySqlUtil.selectSql(sqlNew);
			List<Product> products=null;
			if(sqlList.size()>0) {
				products=new ArrayList<Product>();
				for(int i=0;i<sqlList.size();i++) {
					Product product=new Product();
					product.setProductId(SqlToString.sqlToString(sqlList.get(i).get("product_id")));
					product.setProductName(SqlToString.sqlToString(sqlList.get(i).get("product_name")));
					product.setProductPrice(SqlToString.sqlToString(sqlList.get(i).get("product_price")));
					product.setProductCount(SqlToString.sqlToString(sqlList.get(i).get("product_count")));
					product.setProductStastus(SqlToString.sqlToString(sqlList.get(i).get("product_status")));
					product.setProductSales(SqlToString.sqlToString(sqlList.get(i).get("product_sales")));
					product.setProductDate(SqlToString.sqlToString(sqlList.get(i).get("product_date")));
					product.setProductCategory(SqlToString.sqlToString(sqlList.get(i).get("category_name")));
					product.setProductClassId(SqlToString.sqlToString(sqlList.get(i).get("class_name")));
					product.setProductBrand(SqlToString.sqlToString(sqlList.get(i).get("product_brand_id")));
					product.setProductPhoto(SqlToString.sqlToString(sqlList.get(i).get("product_photo")));
					products.add(product);
				}	
			}
			pagePriceAll.setList(products);
			String sqlCount=pagePriceAll.toCountSql();
			List<HashMap<String,Object>> countList=MySqlUtil.selectSql(sqlCount);
			HashMap<String, Object> map=countList.get(0);
			String count=SqlToString.sqlToString(map.get("count"));
			pagePriceAll.setTotal(Integer.valueOf(count));
			pagePriceAll.setTotalPage(pagePriceAll.getTotalPage());
		} catch (Exception e) {
			e.printStackTrace();	
		}finally {
			MySqlUtil.close();
		}
		return pagePriceAll;
	}
}
