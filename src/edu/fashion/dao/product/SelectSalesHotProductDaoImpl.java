package edu.fashion.dao.product;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import edu.fashion.model.Product;
import edu.fashion.util.MySqlUtil;
import edu.fashion.util.SqlToString;
/**
 *  热门查询 限制四条 销量排序
 * @author 李政奇
 *
 */
public class SelectSalesHotProductDaoImpl {
	public List<Product> selectPADI() {
		String sql="SELECT p.*,category_name,class_name from product p LEFT JOIN category on product_category_id=category_id LEFT JOIN class on product_class_id=class_id where product_status='1' ORDER BY p.product_sales DESC LIMIT 0,4";
		List<Product> products=null;
		try {
			List<HashMap<String, Object>> list =MySqlUtil.selectSql(sql);
			if(list.size()>0) {
				products=new ArrayList<Product>();
				for(int i=0;i<list.size();i++) {
					Product product=new Product();
					product.setProductId(SqlToString.sqlToString(list.get(i).get("product_id")));
					product.setProductName(SqlToString.sqlToString(list.get(i).get("product_name")));
					product.setProductPrice(SqlToString.sqlToString(list.get(i).get("product_price")));
					product.setProductCount(SqlToString.sqlToString(list.get(i).get("product_count")));
					product.setProductStastus(SqlToString.sqlToString(list.get(i).get("product_status")));
					product.setProductSales(SqlToString.sqlToString(list.get(i).get("product_sales")));
					product.setProductDate(SqlToString.sqlToString(list.get(i).get("product_date")));
					product.setProductCategory(SqlToString.sqlToString(list.get(i).get("category_name")));
					product.setProductClassId(SqlToString.sqlToString(list.get(i).get("class_name")));
					product.setProductBrand(SqlToString.sqlToString(list.get(i).get("product_brand_id")));
					product.setProductPhoto(SqlToString.sqlToString(list.get(i).get("product_photo")));
					products.add(product);
				}	
			}
		} catch (Exception e) {
			e.printStackTrace();	
		}finally {
			MySqlUtil.close();
		}
		return products;
	}
}
