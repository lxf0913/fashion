package edu.fashion.dao.product;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import edu.fashion.model.Product;
import edu.fashion.util.MySqlUtil;
import edu.fashion.util.SqlToString;

/**
 * 订单详情产品模板
 * @author 李政奇
 *
 */
public class SelectOrderIdProductDetailDaoImpl {
	public List<Product> orderIdProductList (String orderId) {
		String sql="SELECT * FROM (SELECT * FROM `order` o LEFT JOIN shopcar s ON o.order_id = s.shopcar_order_id) os LEFT JOIN product p ON os.shopcar_product_id =p.product_id WHERE order_id=? AND product_status='1' ";
		List<HashMap<String,Object>> sqlList=MySqlUtil.selectSql(sql, orderId);
		List<Product> productList=new ArrayList<Product>();
		try {
			if(sqlList.size()>0) {
				for (int i = 0; i < sqlList.size(); i++) {
					HashMap<String, Object> map=sqlList.get(i);
					Product	product=new Product();
					product.setProductId(SqlToString.sqlToString2(map.get("product_id")));
					product.setProductName(SqlToString.sqlToString2(map.get("product_name")));
					product.setProductPrice(SqlToString.sqlToString2(map.get("product_price")));
					product.setProductCount(SqlToString.sqlToString2(map.get("product_count")));
					product.setProductStastus(SqlToString.sqlToString2(map.get("product_status")));
					product.setProductSales(SqlToString.sqlToString2(map.get("product_sales")));
					product.setProductDate(SqlToString.sqlToString2(map.get("product_date")));
					product.setProductCategory(SqlToString.sqlToString2(map.get("category_name")));
					product.setProductClassId(SqlToString.sqlToString2(map.get("class_name")));
					product.setProductBrand(SqlToString.sqlToString2(map.get("product_brand_id")));
					product.setProductPhoto(SqlToString.sqlToString2(map.get("product_photo")));
					productList.add(product);
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}finally {
			MySqlUtil.close();
		}
		return productList;
	}
}
