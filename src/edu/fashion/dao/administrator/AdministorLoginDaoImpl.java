package edu.fashion.dao.administrator;

import java.util.HashMap;
import java.util.List;
import edu.fashion.model.Administrator;
import edu.fashion.util.MySqlUtil;
/*
 * lxf
 */
public class AdministorLoginDaoImpl {
	public Administrator login(String name,String password) {
		Administrator a=null;
		String sql="select * from administrator where administrator_name=? and administrator_password=?";
		try {
			List<HashMap<String, Object>> list =MySqlUtil.selectSql(sql,name,password);
			if(list.size()>0) {
				a=new Administrator();
				a.setAdministratorId(list.get(0).get("administrator_id")==null?null:list.get(0).get("administrator_id").toString());
				a.setAdministratorPasswrod(list.get(0).get("administrator_password")==null?null:list.get(0).get("administrator_password").toString());
				a.setAdministratorName(list.get(0).get("administrator_name")==null?null:list.get(0).get("administrator_name").toString());
			}
		} catch (Exception e) {
			e.printStackTrace();	
		}finally {
			MySqlUtil.close();
		}
		return a;
	}
}
