package edu.fashion.dao.adminproduct;

import edu.fashion.util.MySqlUtil;

public class AddproductDaolmpl {
	public int addproductdao(String productname,String productprice,String productcategoryid,String productcount,String productclassid,String productphoto) {
		String sql="insert into product(product_name,product_price,product_category_id,product_count,product_class_id,product_photo)values(?,?,?,?,?,?)";
		int i=MySqlUtil.updateSql(sql,productname,productprice,productcategoryid,productcount,productclassid,productphoto);		
		MySqlUtil.close();
		return i;		
	}
}
