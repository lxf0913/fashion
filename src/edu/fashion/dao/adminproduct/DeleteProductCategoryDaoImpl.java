package edu.fashion.dao.adminproduct;

import edu.fashion.util.MySqlUtil;

//删除商品分类（类如删除西服分类）  chao
public class DeleteProductCategoryDaoImpl {
	public int deleteproductcategory(String params) {
		String sql="delete from category where category_name=?";
		int i=MySqlUtil.updateSql(sql,params);	
		MySqlUtil.close();
		return i;		
	}
}
