package edu.fashion.dao.background;

import edu.fashion.util.MySqlUtil;

/**
 * 根据产品ID删除指定产品
 * @author 李政奇
 *
 */
public class DelectIdProductDaoImpl {
	public int delectProduct(String productId) {
		String sql="DELETE from product WHERE product_id=?";
		int i=MySqlUtil.updateSql(sql,productId);
		MySqlUtil.close();
		return i;
		}
}
