package edu.fashion.dao.background;

import edu.fashion.util.MySqlUtil;

/**
 * 根据订单号修改是否发货
 * @author 李政奇
 *
 */
public class UPdateDeliverDaoImpl {
	public int updateDeliverDI(String orderId) {
		String sql="UPDATE  `order` SET order_isdeliver='1' WHERE order_id=?";
		int i=MySqlUtil.updateSql(sql, orderId);
		MySqlUtil.close();
		return i;
	}
}
