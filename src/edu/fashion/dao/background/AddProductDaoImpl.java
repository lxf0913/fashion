package edu.fashion.dao.background;

import edu.fashion.util.MySqlUtil;

/**
 * 添加商品  注意 产品的大类别和小类别的Id是在Service上插入的
 * @author 李政奇
 *
 */
public class AddProductDaoImpl {
	public int addProduct(String productNmae,String productPrice,String productCategoryId,String productCount,String productClassId,String productPhoto) {
		String sql="INSERT INTO product (product_name,product_price,product_category_id,product_count,product_class_id,product_photo) VALUES(?,?,?,?,?,?)";
		int i= MySqlUtil.updateSql(sql,productNmae,productPrice ,productCategoryId,productCount,productClassId,productPhoto);
		MySqlUtil.close();
		return i;
	}
}
