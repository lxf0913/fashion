package edu.fashion.dao.background;

import java.util.HashMap;
import java.util.List;

import edu.fashion.model.Category;
import edu.fashion.util.MySqlUtil;
import edu.fashion.util.SqlToString;

public class SelectCategoryOneDaoImpl {
	public Category addCategory(String categoryName) {
		String sql="SELECT * from category WHERE category_name=?";
		List<HashMap<String, Object>> sqlList=MySqlUtil.selectSql(sql, categoryName);
		Category category =null;
		if(sqlList.size()>0) {
			HashMap<String, Object> map =sqlList.get(0);
			category =new Category();
			category.setCategoryId(SqlToString.sqlToString(map.get("category_id")));
		}
		MySqlUtil.close();
		return category;
	}
}
