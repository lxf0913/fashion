package edu.fashion.dao.background;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import edu.fashion.model.Order;
import edu.fashion.model.PageModel;
import edu.fashion.model.User;
import edu.fashion.util.MySqlUtil;
import edu.fashion.util.SqlToString;
/**
 * 根据用户名 模糊查询订单 并按时间 降序 分页
 * @author 李政奇
 *
 */
public class SelectLikeUserNameOrderDaoImpl {
	public PageModel<Order> like(String username,String page,String pageSize){
		if (page==null) {
			page="1";
		}
		String userName=username;
		String sqlOld="SELECT * FROM `order` LEFT JOIN `user` ON `order`.order_user_id=`user`.user_id  WHERE user_account LIKE"+"'%"+userName+"%'"+"ORDER BY order_date DESC";
		PageModel<Order> pageAll=new PageModel<Order>(sqlOld, page, pageSize);
		String sqlNew=pageAll.toMysqlSql();
		try {
			List<HashMap<String, Object>> sqlList =MySqlUtil.selectSql(sqlNew);
			List<Order> orderList=null;
			if(sqlList.size()>0) {
				orderList=new ArrayList<Order>();
				for(int i=0;i<sqlList.size();i++) {
					HashMap<String, Object> map=sqlList.get(i);
					Order order=new Order();
					User user=new User();
					order.setOrderAddress(SqlToString.sqlToString(map.get("order_address")));
					order.setOrderComment(SqlToString.sqlToString(map.get("order_comment")));
					order.setOrderDate(SqlToString.sqlToString(map.get("order_date")));
					order.setOrderId(SqlToString.sqlToString(map.get("order_id")));
					order.setOrderIsdeliver(SqlToString.sqlToString(map.get("order_isdeliver")));
					order.setOrderIspay(SqlToString.sqlToString(map.get("order_ispay")));
					order.setOrderPrice(SqlToString.sqlToString(map.get("order_price")));
					order.setOrderStatus(SqlToString.sqlToString(map.get("order_status")));
					order.setOrderUserId(SqlToString.sqlToString(map.get("order_user_id")));
					user.setUserAccount(SqlToString.sqlToString(map.get("user_account")));
					user.setUserEmail(SqlToString.sqlToString(map.get("user_email")));
					user.setUserHead(SqlToString.sqlToString(map.get("user_head")));
					user.setUserId(SqlToString.sqlToString(map.get("user_id")));
					user.setUserName(SqlToString.sqlToString(map.get("user_name")));
					user.setUserPasswrod(SqlToString.sqlToString(map.get("user_password")));
					user.setUserSex(SqlToString.sqlToString(map.get("user_sex")));
					user.setUserTelephone(SqlToString.sqlToString(map.get("user_telephone")));
					user.setUserTime(SqlToString.sqlToString(map.get("user_time")));
					user.setAddress(SqlToString.sqlToString(map.get("user_address")));
					order.setOrderUser(user);
					orderList.add(order);
				}	
			}
			pageAll.setList(orderList);
			String sqlCount=pageAll.toCountSql();
			List<HashMap<String,Object>> countList=MySqlUtil.selectSql(sqlCount);
			HashMap<String, Object> map=countList.get(0);
			String count=SqlToString.sqlToString(map.get("count"));
			pageAll.setTotal(Integer.valueOf(count));
			pageAll.setTotalPage(pageAll.getTotalPage());
		} catch (Exception e) {
			e.printStackTrace();	
		}finally {
			MySqlUtil.close();
		}
		return pageAll;
	}
}
