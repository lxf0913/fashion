package edu.fashion.dao.background;

import edu.fashion.util.MySqlUtil;

/**
 * 根据产品	ID 修改 产品信息
 * @author 李政奇
 *
 */
public class UpdateIdProductDaoImpl {
	public int updateProduct(String productName,String productPrice,String productStatus,String productPhoto,String productId,String classId,String categoryId,String count) {
		String sql="UPDATE product SET product_name=?,product_price=?,product_status=?,product_photo=?,product_class_id=?,product_category_id=?,product_count=?  WHERE product_id=?";
		int i=0;
		try {
			i=MySqlUtil.updateSql(sql, productName,productPrice,productStatus,productPhoto,classId,categoryId,count,productId);
		} catch (Exception e) {
			
		}
		MySqlUtil.close();
		return i;
	}
}
