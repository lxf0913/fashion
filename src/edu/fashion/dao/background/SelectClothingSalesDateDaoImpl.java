package edu.fashion.dao.background;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import edu.fashion.model.Category;
import edu.fashion.model.Product;
import edu.fashion.util.MySqlUtil;
import edu.fashion.util.SqlToString;

/**
 * 返回小类别名 按销量排序 与 后面小类别的销量相对应
 * @author 李政奇
 *
 */
public class SelectClothingSalesDateDaoImpl {  
	public List<List<Object>> selectCategoryDI() {
		String sql="SELECT sum(pc.product_sales) product_sales, pc.category_name category_name FROM(SELECT* FROM product LEFT JOIN category ON product_category_id=category_id) pc GROUP BY pc.category_name";
		List<List<Object>> data =new ArrayList<List<Object>>();
		List<Object> categoryList =new ArrayList<Object>();
		List<Object> productList =new ArrayList<Object>();
		try {
			List<HashMap<String,Object>> sqlList=MySqlUtil.selectSql(sql);
			for (int i = 0; i < sqlList.size();i++) {
				HashMap<String,Object> map=sqlList.get(i);
				Category category=new Category();
				Product product=new Product();
				category.setCategoryName(SqlToString.sqlToString2(map.get("category_name")));
				product.setProductSales(SqlToString.sqlToString2(map.get("product_sales")));
				categoryList.add(category);
				productList.add(product);
			}
			data.add(categoryList);
			data.add(productList);
		} catch (Exception e) {
			e.printStackTrace();	
		}finally {
			MySqlUtil.close();
		}
		return data;
		}
}
