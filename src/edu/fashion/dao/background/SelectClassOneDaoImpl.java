package edu.fashion.dao.background;

import java.util.HashMap;
import java.util.List;

import edu.fashion.model.Classes;
import edu.fashion.util.MySqlUtil;
import edu.fashion.util.SqlToString;

public class SelectClassOneDaoImpl {
	public Classes addClass(String className) {
		String sql="SELECT * from class WHERE class_name=?";
		Classes classes=null;
		try {
			List<HashMap<String, Object>> sqlList=MySqlUtil.selectSql(sql, className);
			if(sqlList.size()>0) {
				HashMap<String, Object> map =sqlList.get(0);
				classes=new Classes();
				classes.setClassId(SqlToString.sqlToString(map.get("class_id")));
			}
		} catch (Exception e) {
		}
		MySqlUtil.close();
		return classes;
	}
}
