package edu.fashion.dao.shopcar;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import edu.fashion.model.ShopCar;
import edu.fashion.util.MySqlUtil;
import edu.fashion.util.SqlToString;

//肖赵阳
//返回一组商品

public class SelectShopDaolmpl {
	public List<ShopCar> selectshop(String userid) {
		List<ShopCar> shops = new ArrayList<ShopCar>();
		String sql = "select * from shopcar A left join product B on A.shopcar_product_id=B.product_id where A.shopcar_status='1' and A.shopcar_user_id=? ";
		try {
			List<HashMap<String, Object>> selectlist = MySqlUtil.selectSql(sql, userid);
			if (selectlist.size() > 0) {
				for (int i = 0; i < selectlist.size(); i++) {
					HashMap<String, Object> map = selectlist.get(i);
					ShopCar shop = new ShopCar();
					shop.setShopcarId(SqlToString.sqlToString(map.get("shopcar_id")));
					shop.setShopcarProductId(SqlToString.sqlToString(map.get("shopcar_product_id")));
					shop.setShopcarUserId(SqlToString.sqlToString(map.get("shopcar_user_id")));
					shop.setShopcarOrderId(SqlToString.sqlToString(map.get("shopcar_order_id")));
					shop.setShopcarDate(SqlToString.sqlToString(map.get("shopcar_date")));
					shop.setShopcarStatus(SqlToString.sqlToString(map.get("shopcar_status")));
					shop.setShopcarNumber(SqlToString.sqlToString(map.get("shopcar_number")));
					shop.setShopPrice(SqlToString.sqlToString(map.get("shopcar_price")));
					shop.setShopComment(SqlToString.sqlToString(map.get("shopcar_comment")));
					shop.setShopColor(SqlToString.sqlToString(map.get("shop_color")));
					shop.setShopSize(SqlToString.sqlToString(map.get("shop_size")));
					shop.setProductname(SqlToString.sqlToString(map.get("product_name")));
					shop.setProductphoto(SqlToString.sqlToString(map.get("product_photo")));
					shop.setShopPrice(SqlToString.sqlToString(map.get("shop_price")));
					shops.add(shop);
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			MySqlUtil.close();
		}
		return shops;
	}
}
