package edu.fashion.dao.shopcar;

import edu.fashion.util.MySqlUtil;

public class InsertShopDaolmpl {
	public int insertshop(String userid, String productid, String productnumber, String shopprice,String color,String size) {
		String sql = "INSERT INTO shopcar (shopcar_product_id,shopcar_user_id,shopcar_number,shop_price,shop_color,shop_size) VALUES (?,?,?,?,?,?)";
		int i = MySqlUtil.updateSql(sql, productid, userid, productnumber, shopprice,color,size);
		MySqlUtil.close();
		return i;
	}
}
