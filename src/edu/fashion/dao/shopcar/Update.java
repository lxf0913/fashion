package edu.fashion.dao.shopcar;

import edu.fashion.util.MySqlUtil;

public class Update{
	public int update(String shopid,String orderid){
		String sql="UPDATE shopcar SET shopcar_order_id=?,shopcar_status='2' where shopcar_id=? ";
		int i=0;
		try {
			i=MySqlUtil.updateSql(sql,orderid,shopid);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			MySqlUtil.close();
		}
		return i;
	}
}
