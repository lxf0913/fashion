package edu.fashion.dao.user;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import edu.fashion.model.PageModel;
import edu.fashion.model.User;
import edu.fashion.util.MySqlUtil;
import edu.fashion.util.SqlToString;
/**
 * 查询所有用户
 * 按时间降序
 * @author 李政奇
 *
 */

public class SelectDateUserAllDaoImpl {
	public PageModel<User> selectDateAllUser(String page){
		if (page==null) {
			page="1";
		}
		String sqlOld="SELECT * FROM `user` ORDER BY `user`.user_time DESC";
		String pageSize="7";
		PageModel<User> pageDateAll=new PageModel<User>(sqlOld, page, pageSize);
		String sqlNew=pageDateAll.toMysqlSql();
		try {
			List<User> userList=null;
			List<HashMap<String, Object>> sqlList =MySqlUtil.selectSql(sqlNew);
			if(sqlList.size()>0) {
				userList=new ArrayList<User>();
				for(int i=0;i<sqlList.size();i++) {
					HashMap<String,Object> map=sqlList.get(i);
					User user=new User();
					user.setUserAccount(SqlToString.sqlToString(map.get("user_account")));
					user.setUserEmail(SqlToString.sqlToString(map.get("user_email")));
					user.setUserHead(SqlToString.sqlToString(map.get("user_head")));
					user.setUserId(SqlToString.sqlToString(map.get("user_id")));
					user.setUserName(SqlToString.sqlToString(map.get("user_name")));
					user.setUserPasswrod(SqlToString.sqlToString(map.get("user_password")));
					user.setUserSex(SqlToString.sqlToString(map.get("user_sex")));
					user.setUserTelephone(SqlToString.sqlToString(map.get("user_telephone")));
					user.setUserTime(SqlToString.sqlToString(map.get("user_time")));
					user.setAddress(SqlToString.sqlToString(map.get("user_address")));
					userList.add(user);
				}	
			}
			pageDateAll.setList(userList);
			String sqlCount=pageDateAll.toCountSql();
			List<HashMap<String,Object>> countList=MySqlUtil.selectSql(sqlCount);
			HashMap<String, Object> map=countList.get(0);
			String count=SqlToString.sqlToString(map.get("count"));
			pageDateAll.setTotal(Integer.valueOf(count));
			pageDateAll.setTotalPage(pageDateAll.getTotalPage());
		} catch (Exception e) {
			e.printStackTrace();	
		}finally {
			MySqlUtil.close();
		}
		return pageDateAll;
	}
}
