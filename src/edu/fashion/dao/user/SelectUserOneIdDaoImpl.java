package edu.fashion.dao.user;

import java.util.HashMap;
import java.util.List;

import edu.fashion.model.User;
import edu.fashion.util.MySqlUtil;
import edu.fashion.util.SqlToString;

public class SelectUserOneIdDaoImpl {
	public User userOne(String userId) {
		String sql ="SELECT * FROM `user` WHERE user_id=?";
		List<HashMap<String,Object>> sqlList=MySqlUtil.selectSql(sql, userId);
		User user=null;
		try {
			if(sqlList.size()>0) {
				HashMap<String, Object> map=sqlList.get(0);
				user=new User();
				user.setUserAccount(SqlToString.sqlToString(map.get("user_account")));
				user.setUserEmail(SqlToString.sqlToString(map.get("user_email")));
				user.setUserHead(SqlToString.sqlToString(map.get("user_head")));
				user.setUserId(SqlToString.sqlToString(map.get("user_id")));
				user.setUserName(SqlToString.sqlToString(map.get("user_name")));
				user.setUserPasswrod(SqlToString.sqlToString(map.get("user_password")));
				user.setUserSex(SqlToString.sqlToString(map.get("user_sex")));
				user.setUserTelephone(SqlToString.sqlToString(map.get("user_telephone")));
				user.setUserTime(SqlToString.sqlToString(map.get("user_time")));
				user.setAddress(SqlToString.sqlToString(map.get("user_address")));
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}finally {
			MySqlUtil.close();
		}
		return user;
	} 
}
