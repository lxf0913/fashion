package edu.fashion.dao.user;

import java.util.HashMap;
import java.util.List;

import edu.fashion.model.User;
import edu.fashion.util.MySqlUtil;
import edu.fashion.util.SqlToString;

/*
 * lxf
 */
public class UserMyDetailDaoImpl {
	public User userDtail(String userid) {
		User user=null;
		String sql="select * from user where  user_id=? ";
		try {
		List<HashMap<String,Object>> loginlist=MySqlUtil.selectSql(sql,userid);
		if(loginlist.size()>0){
			HashMap<String,Object> map=loginlist.get(0);
			user =new User();
			user.setUserAccount(SqlToString.sqlToString(map.get("user_account")));
			user.setUserEmail(SqlToString.sqlToString(map.get("user_email")));
			user.setUserHead(SqlToString.sqlToString(map.get("user_head")));
			user.setUserId(SqlToString.sqlToString(map.get("user_id")));
			user.setUserName(SqlToString.sqlToString(map.get("user_name")));
			user.setUserPasswrod(SqlToString.sqlToString(map.get("user_password")));
			user.setUserSex(SqlToString.sqlToString(map.get("user_sex")));
			user.setUserTelephone(SqlToString.sqlToString(map.get("user_telephone")));
			user.setUserTime(SqlToString.sqlToString(map.get("user_time")));
			user.setAddress(SqlToString.sqlToString(map.get("user_address")));
		}
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			MySqlUtil.close();
		}
		return user;
	}
}
