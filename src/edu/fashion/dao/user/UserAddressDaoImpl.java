package edu.fashion.dao.user;

import java.util.HashMap;
import java.util.List;

import edu.fashion.model.Address;
import edu.fashion.util.MySqlUtil;
import edu.fashion.util.SqlToString;

public class UserAddressDaoImpl {
	public Address selectMyAddress(String userid) {
		Address address=null;
		String sql="select * from address where  address_user_id=? ";
		try {
		List<HashMap<String,Object>> loginlist=MySqlUtil.selectSql(sql,userid);
		if(loginlist.size()>0){
			HashMap<String,Object> map=loginlist.get(0);
			address =new Address();
			address.setAddressLocation(SqlToString.sqlToString(map.get("address_location")));
		}
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			MySqlUtil.close();
		}
		return address;
	}
}
