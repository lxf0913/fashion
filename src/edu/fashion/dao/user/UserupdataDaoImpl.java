package edu.fashion.dao.user;
import edu.fashion.util.MySqlUtil;

public class UserupdataDaoImpl {
	public int updata(String address,String useremail,String usertelephone,String usersex,String userhead,String userid,String username) {
		String sql="UPDATE  user set user_address=?,user_email=?,user_telephone=?,user_sex=?,user_head=?,user_name=? where user_id=?";
		int i=0;
		try {
			i=MySqlUtil.updateSql(sql,address,useremail,usertelephone,usersex,userhead,username,userid);
			
		} catch (Exception e) {
			e.printStackTrace();	
		}finally {
			MySqlUtil.close();
		}
		return i;
	}
}
