package edu.fashion.dao.user;

import edu.fashion.util.MySqlUtil;

/**
 * 根据用户ID删除用户
 * @author 李政奇
 *
 */
public class DeleteUserDaoImpl {
	public int deleteUserDI(String userId) {
		String sql="DELETE FROM `user` WHERE `user`.user_id = ?";
		int i=MySqlUtil.updateSql(sql, userId);
		MySqlUtil.close();
		return i;
	}
}
