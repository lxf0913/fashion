package edu.fashion.common;

import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import com.google.gson.Gson;

public class JsonResultWriter {
	public static void writer(HttpServletResponse response,Object object) throws IOException {
		Gson gson=new Gson();
		String json=gson.toJson(object);
		response.setContentType("application/json");
		response.getWriter().print(json);
		response.getWriter().close();
	}
}
