package edu.fashion.api.background;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import edu.fashion.common.Constants;
import edu.fashion.common.JsonResult;
import edu.fashion.common.JsonResultWriter;
import edu.fashion.service.background.UpdateIdServiceImpl;

/**根据Id修改产品数据
 * Servlet implementation class upDateIdProductServlet
 */
@WebServlet("/background/UpDateIdProduct")
public class UpDateIdProductServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpDateIdProductServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String productNmae=request.getParameter("productNmae");
		String productPrice=request.getParameter("productPrice");
		String productStatus=request.getParameter("productStatus");
		String productPhoto=request.getParameter("productPhoto");
		String productclass=request.getParameter("productclass");
		String productcategory=request.getParameter("productcategory");
		String productId=request.getParameter("productId");
		String productcount=request.getParameter("productcount");
		JsonResult<Integer> result=null;
		try {
			UpdateIdServiceImpl update=new UpdateIdServiceImpl();
			int i=update.upDateId(productNmae, productPrice, productStatus, productPhoto, productId,productclass,productcategory,productcount);
			if(i>0) {
				result=new JsonResult<Integer> (Constants.STATUS_SUCCESS,"修改成功",i);
			}else {
				result=new JsonResult<Integer> (Constants.STATUS_UNFOUND,"没有数据");
			}
		} catch (Exception e) {
				result=new JsonResult<Integer>(Constants.STATUS_FAILURE,"修改异常",e.getMessage());
		}
		JsonResultWriter.writer(response, result);
	}
}
