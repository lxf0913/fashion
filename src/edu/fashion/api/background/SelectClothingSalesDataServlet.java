package edu.fashion.api.background;

import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import edu.fashion.common.Constants;
import edu.fashion.common.JsonResult;
import edu.fashion.common.JsonResultWriter;
import edu.fashion.service.background.SelectClothingSalesDataServiceImpl;

/**
 * 返回小类别名 按销量排序 与 后面小类别的销量相对应
 * @author 李政奇
 * Servlet implementation class SelectClothingSalesDataServlet
 */
@WebServlet("/background/ClothingSalesData")
public class SelectClothingSalesDataServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SelectClothingSalesDataServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		JsonResult<List<List<Object>>> result=null;
		try {
			SelectClothingSalesDataServiceImpl clothingSales=new SelectClothingSalesDataServiceImpl();
			List<List<Object>> list= clothingSales.clothingSales();
			if(list.size()>0) {
				result=new JsonResult<List<List<Object>>> (Constants.STATUS_SUCCESS,"查询成功",list);
			}else {
				result=new JsonResult<List<List<Object>>> (Constants.STATUS_UNFOUND,"没有数据");
			}
		} catch (Exception e) {
				result=new JsonResult<List<List<Object>>>(Constants.STATUS_FAILURE,"查询异常",e.getMessage());
		}
		JsonResultWriter.writer(response, result);
	}
}
