package edu.fashion.api.background;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import edu.fashion.common.Constants;
import edu.fashion.common.JsonResult;
import edu.fashion.common.JsonResultWriter;

import edu.fashion.service.background.UPdateDeliverServiceImpl;

/**
 * Servlet implementation class UPdateDeliverServlet
 */
@WebServlet("/background/UPdateDeliver")
public class UPdateDeliverServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UPdateDeliverServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String orderId=request.getParameter("orderId");
		JsonResult<Integer> result=null;
		try {
			UPdateDeliverServiceImpl update =new UPdateDeliverServiceImpl();
			int i=update.updateDeliverSI(orderId);
			if(i>0) {
				result=new JsonResult<Integer> (Constants.STATUS_SUCCESS,"成功",i);
			}else {
				result=new JsonResult<Integer> (Constants.STATUS_UNFOUND,"没有数据");
			}
		} catch (Exception e) {
				result=new JsonResult<Integer>(Constants.STATUS_FAILURE,"异常",e.getMessage());
		}
		JsonResultWriter.writer(response, result);
	}

}
