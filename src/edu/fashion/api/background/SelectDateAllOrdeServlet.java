package edu.fashion.api.background;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import edu.fashion.common.Constants;
import edu.fashion.common.JsonResult;
import edu.fashion.common.JsonResultWriter;
import edu.fashion.model.Order;
import edu.fashion.model.PageModel;
import edu.fashion.service.background.SelectDateAllOrderServiceImpl;

/**查询订单所有 按时间降序
 * 李政奇
 * Servlet implementation class SelectDateAllOrdeServlet
 */
@WebServlet("/background/DateAllOrde")
public class SelectDateAllOrdeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SelectDateAllOrdeServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String page = request.getParameter("page");
		String pageSize =request.getParameter("pageSize");
		JsonResult<PageModel<Order>> result=null;
		try{
			SelectDateAllOrderServiceImpl pageModel=new SelectDateAllOrderServiceImpl();
			PageModel<Order> model =pageModel.selectDateAllOrderSI(page, pageSize) ;
			if(model!=null){
			result=new JsonResult<PageModel<Order>>(Constants.STATUS_SUCCESS,"查询成功",model);
		}else{
			result=new JsonResult<PageModel<Order>>(Constants.STATUS_UNFOUND,"查询失败");
		}}catch(Exception e){
			result=new JsonResult<PageModel<Order>>(Constants.STATUS_FAILURE,"查询异常",e.getMessage());
		}
			JsonResultWriter.writer(response, result);
	}

}
