package edu.fashion.api.background;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import edu.fashion.common.Constants;
import edu.fashion.common.JsonResult;
import edu.fashion.common.JsonResultWriter;
import edu.fashion.service.background.AddProductServiceImpl;

/**
 * 李政奇
 * 添加商品
 * Servlet implementation class AddProductServlet
 */
@WebServlet("/background/AddProduct")
public class AddProductServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddProductServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String productNmae=request.getParameter("productNmae");
		String productPrice=request.getParameter("productPrice");
		String categoryName=request.getParameter("categoryName");
		String productCount=request.getParameter("productCount");
		String className=request.getParameter("className");
		String productPhoto=request.getParameter("productPhoto");
		JsonResult<Integer> result=null;
		try {
			AddProductServiceImpl add=new AddProductServiceImpl();
			int i=add.addProduct(productNmae, productPrice, categoryName, productCount, className, productPhoto);
			if(i>0) {
				result=new JsonResult<Integer> (Constants.STATUS_SUCCESS,"添加成功",i);
			}else {
				result=new JsonResult<Integer> (Constants.STATUS_UNFOUND,"没有数据");
			}
		} catch (Exception e) {
				result=new JsonResult<Integer>(Constants.STATUS_FAILURE,"添加异常",e.getMessage());
		}
		JsonResultWriter.writer(response, result);
	}
}
