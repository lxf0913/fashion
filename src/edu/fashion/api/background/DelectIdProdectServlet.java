package edu.fashion.api.background;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import edu.fashion.common.Constants;
import edu.fashion.common.JsonResult;
import edu.fashion.common.JsonResultWriter;
import edu.fashion.service.background.DelectIdProductServiceImpl;

/**根据商品Id删除商品
 * 李政奇
 * Servlet implementation class DelectIdProdectImpl
 */
@WebServlet("/background/DelectIdProdect")
public class DelectIdProdectServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DelectIdProdectServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String productId=request.getParameter("prodectId");
		JsonResult<Integer> result=null;
		try {
			DelectIdProductServiceImpl delect=new DelectIdProductServiceImpl();
			int i= delect.delectId(productId);
			if(i>0) {
				result=new JsonResult<Integer> (Constants.STATUS_SUCCESS,"删除成功",i);
			}else {
				result=new JsonResult<Integer> (Constants.STATUS_UNFOUND,"没有数据");
			}
		} catch (Exception e) {
				result=new JsonResult<Integer>(Constants.STATUS_FAILURE,"删除异常",e.getMessage());
		}
		JsonResultWriter.writer(response, result);
	}
}
