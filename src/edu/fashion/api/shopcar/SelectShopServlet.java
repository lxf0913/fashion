package edu.fashion.api.shopcar;

import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import edu.fashion.common.Constants;
import edu.fashion.common.JsonResult;
import edu.fashion.common.JsonResultWriter;
import edu.fashion.service.shopcar.SelectShopcarServicelmpl;
//肖赵阳
//查询购物车商品通过userID
/**
 * Servlet implementation class SelectShopServlet
 */

@WebServlet("/SelectShopServlet")
public class SelectShopServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String userid=request.getParameter("userid");
		JsonResult result =null;

		SelectShopcarServicelmpl selectallshop = new SelectShopcarServicelmpl();
			try {
				List shops= selectallshop.selectshopservice(userid);
				if(shops.size()!=0){
					result=new JsonResult(Constants.STATUS_SUCCESS,"查询成功",shops);
				}else {
					result=new JsonResult(Constants.STATUS_UNFOUND,"用户购物车没有加入商品");
				}
			} catch (Exception e) {
				// TODO: handle exception
				result =new JsonResult(Constants.STATUS_FAILURE, "查询购物车商品异常", e.getMessage());
			}
			JsonResultWriter.writer(response, result);

	}

}