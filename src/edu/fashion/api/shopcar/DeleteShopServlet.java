package edu.fashion.api.shopcar;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import edu.fashion.common.Constants;
import edu.fashion.common.JsonResult;
import edu.fashion.common.JsonResultWriter;
import edu.fashion.service.shopcar.DeleteShopcarServicelmpl;

/**
 * Servlet implementation class DeleteShopServlet
 */
@WebServlet("/DeleteShopServlet")
public class DeleteShopServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;   
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String shopcarid = request.getParameter("shopcarid");
		JsonResult result = null;
		try {
			DeleteShopcarServicelmpl dssl = new DeleteShopcarServicelmpl();
			int i = dssl.deleteshops(shopcarid);
			if (i != 0) {
				result = new JsonResult(Constants.STATUS_SUCCESS, "�Ƴ��ɹ�", i);
			} else {
				result = new JsonResult(Constants.STATUS_UNFOUND, "�Ƴ�ʧ��");
			}
		} catch (Exception e) {
			result = new JsonResult(Constants.STATUS_FAILURE, "�Ƴ��쳣", e.getMessage());
		}
		JsonResultWriter.writer(response, result);
	}

}
