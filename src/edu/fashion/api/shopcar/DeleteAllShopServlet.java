package edu.fashion.api.shopcar;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import edu.fashion.common.Constants;
import edu.fashion.common.JsonResult;
import edu.fashion.common.JsonResultWriter;
import edu.fashion.service.shopcar.DeleteAllShopcarServicelmpl;


/**
 * Servlet implementation class DeleteAllShopServlet
 */
@WebServlet("/DeleteAllShopServlet")
public class DeleteAllShopServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String userid = request.getParameter("userid");
		JsonResult result = null;
		try {
			DeleteAllShopcarServicelmpl dass = new DeleteAllShopcarServicelmpl();
			int i = dass.deleteallshops(userid);
			if (i != 0) {
				result = new JsonResult(Constants.STATUS_SUCCESS, "��ճɹ�", i);
			} else {
				result = new JsonResult(Constants.STATUS_UNFOUND, "�Ƴ�ʧ��");
			}
		} catch (Exception e) {
			result = new JsonResult(Constants.STATUS_FAILURE, "�Ƴ��쳣", e.getMessage());
		}
		JsonResultWriter.writer(response, result);
	}

}
