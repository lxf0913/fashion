package edu.fashion.api.shopcar;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import edu.fashion.common.Constants;
import edu.fashion.common.JsonResult;
import edu.fashion.common.JsonResultWriter;
import edu.fashion.service.shopcar.InsertShopcarServicelmpl;

/**
 * Servlet implementation class InsertShopServlet
 */
@WebServlet("/InsertShopServlet")
public class InsertShopServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		String userid = request.getParameter("userid");
		String productid = request.getParameter("productid");
		String productnumber = request.getParameter("productnumber");
		String shopprice = request.getParameter("shopprice");
		String color = request.getParameter("shopcolor");
		String size = request.getParameter("shopsize");
		JsonResult result = null;
		try {
			InsertShopcarServicelmpl iss = new InsertShopcarServicelmpl();
			int i = iss.insertshopserver(userid, productid, productnumber, shopprice,color,size);
			if (i != 0) {
				result = new JsonResult(Constants.STATUS_SUCCESS, "加入成功", i);
			} else {
				result = new JsonResult(Constants.STATUS_UNFOUND, "用户没有登陆");
			}
		} catch (Exception e) {
			result = new JsonResult(Constants.STATUS_FAILURE, "加入失败", e.getMessage());
		}
		JsonResultWriter.writer(response, result);
	}

}
