package edu.fashion.api.administrator;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import edu.fashion.common.Constants;
import edu.fashion.common.JsonResult;
import edu.fashion.common.JsonResultWriter;
import edu.fashion.service.administrator.AdministratorRegisteredServiceImpl;





@WebServlet("/api/administrator/register")
public class AdministratorRegisteredServlet extends HttpServlet{
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//获取页面信息
		String administratorName=request.getParameter("administratorName");
		String administratorPasswrod=request.getParameter("administratorPasswrod");


		String [] params=new String[2];
		params[0]=administratorName;
		params[1]=administratorPasswrod;

		JsonResult result=null;
		try {
			AdministratorRegisteredServiceImpl serviceImpl=new AdministratorRegisteredServiceImpl();
			int user=serviceImpl.register(params);
			if(user!=0) {
				result=new JsonResult(Constants.STATUS_SUCCESS,"注册成功",user);
			}else {
				result=new JsonResult(Constants.STATUS_UNFOUND,"用户名密码错误");	
			}
		}catch(Exception  e) {
			result=new JsonResult(Constants.STATUS_FAILURE,"查询异常",e.getMessage());
		}
		JsonResultWriter.writer(response, result);	
	}

}
