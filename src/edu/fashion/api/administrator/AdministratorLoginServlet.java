package edu.fashion.api.administrator;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import edu.fashion.common.Constants;
import edu.fashion.common.JsonResult;
import edu.fashion.common.JsonResultWriter;
import edu.fashion.model.Administrator;
import edu.fashion.service.administrator.AdministratorLoginServiceImpl;

/*
 * lxf
 * 管理登录   
 * 账户 ，密码
 */   
  
@WebServlet("/AdministratorLoginServlet")
public class AdministratorLoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String name=request.getParameter("userName");
		String password=request.getParameter("userPassword");
//		String yzm=request.getParameter("yzm");s
		JsonResult<Administrator> result=null;
//		if(request.getSession().getAttribute("code").equals(yzm)) {
			AdministratorLoginServiceImpl al=new AdministratorLoginServiceImpl();
			try {
				Administrator user=al.login(name, password);
				if(user!=null) {
					result=new JsonResult<Administrator>(Constants.STATUS_SUCCESS,"登录成功",user);
				}else {
					result=new JsonResult<Administrator>(Constants.STATUS_UNFOUND,"密码错误或用户不存在");
				}
			} catch (Exception e) {
					result=new JsonResult<Administrator>(Constants.STATUS_FAILURE,"登录异常",e.getMessage());
			}
//		}else {
//			result=new JsonResult<Administrator>(Constants.STATUS_UNFOUND,"验证码不对");
//		}
		JsonResultWriter.writer(response, result);
	}

}
