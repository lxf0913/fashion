package edu.fashion.api.administrator;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import edu.fashion.common.Constants;
import edu.fashion.common.JsonResult;
import edu.fashion.common.JsonResultWriter;
import edu.fashion.service.user.UserRegistrationServicelmp;
//Ф����
/**
 * Servlet implementation class UserRegistrationServlet
 */
@WebServlet("/addUserRegistrationServlet")
public class UserRegistrationServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String useraccount=request.getParameter("useraccount");
		String userpassword=request.getParameter("userpassword");
		String username=request.getParameter("username");
		String useremail=request.getParameter("useremail");
		String usertelephone=request.getParameter("usertelephone");
		String usersex=request.getParameter("usersex");
		String userhead=request.getParameter("userhead");
		String useraddress=request.getParameter("useraddress");
		JsonResult result=null;
		try{
			UserRegistrationServicelmp urs=new UserRegistrationServicelmp();
			int i=urs.userregistrationserver(useraccount, userpassword, username, useremail, usertelephone, usersex, userhead,useraddress);
			if(i!=0){
			result=new JsonResult(Constants.STATUS_SUCCESS,"ע��ɹ�",i);
		}else{
			result=new JsonResult(Constants.STATUS_UNFOUND,"�����û���ע��");
		}}catch(Exception e){
			result=new JsonResult(Constants.STATUS_FAILURE,"ע���쳣",e.getMessage());
		}
			JsonResultWriter.writer(response, result);
	}

}
