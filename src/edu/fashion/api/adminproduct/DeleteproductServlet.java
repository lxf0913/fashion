package edu.fashion.api.adminproduct;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import edu.fashion.common.Constants;
import edu.fashion.common.JsonResult;
import edu.fashion.common.JsonResultWriter;
import edu.fashion.dao.adminproduct.DeleteproductDaolmpl;


/**
 * Servlet implementation class DeleteproductServlet
 */
@WebServlet("/DeleteproductServlet")
public class DeleteproductServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String productid=request.getParameter("productid");
		JsonResult result=null;
		try {
			DeleteproductDaolmpl dpdlpl=new DeleteproductDaolmpl();
			int i=dpdlpl.deleteproducdl(productid);
			if(i!=0) {
				result=new JsonResult(Constants.STATUS_SUCCESS,"ɾ����Ʒ�ɹ�",i);
			}else {
				result=new JsonResult(Constants.STATUS_UNFOUND,"ɾ����Ʒʧ��");	
			}
		}catch(Exception  e) {
			result=new JsonResult(Constants.STATUS_FAILURE,"ɾ���쳣",e.getMessage());
		}
		JsonResultWriter.writer(response, result);	
	}
}
