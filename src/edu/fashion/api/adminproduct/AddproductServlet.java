package edu.fashion.api.adminproduct;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import edu.fashion.common.Constants;
import edu.fashion.common.JsonResult;
import edu.fashion.common.JsonResultWriter;
import edu.fashion.service.adminproduct.AddproductServicelmp;

/**
 * Servlet implementation class AddproductServlet
 */
@WebServlet("/AddproductServlet")
public class AddproductServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String productname=request.getParameter("productname");
		String productprice=request.getParameter("productprice");
		String productcategoryid=request.getParameter("productcategoryid");
		String productcount=request.getParameter("productcount");
		String productclassid=request.getParameter("productclassid");
		String productphoto=request.getParameter("productphoto");
		
		JsonResult result=null;
		try {
			AddproductServicelmp addpslp=new AddproductServicelmp();
			int i=addpslp.addproductservice(productname, productprice, productcategoryid, productcount, productclassid, productphoto);
			if(i!=0) {
				result=new JsonResult(Constants.STATUS_SUCCESS,"添加商品成功",i);
			}else {
				result=new JsonResult(Constants.STATUS_UNFOUND,"添加商品失败");	
			}
		}catch(Exception  e) {
			result=new JsonResult(Constants.STATUS_FAILURE,"添加异常",e.getMessage());
		}
		JsonResultWriter.writer(response, result);	
	}

}
