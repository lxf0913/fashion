package edu.fashion.api.adminproduct;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import edu.fashion.common.Constants;
import edu.fashion.common.JsonResult;
import edu.fashion.common.JsonResultWriter;
import edu.fashion.service.adminproduct.AddProductCategoryServiceImpl;

@WebServlet("/AddProductCategoryServlet")
public class AddProductCategoryServlet extends HttpServlet {
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String categoryname=request.getParameter("categoryname");

		String [] params=new String[1];
		params[0]=categoryname;

		JsonResult result=null;
		try {
			AddProductCategoryServiceImpl serviceImpl=new AddProductCategoryServiceImpl();
			int category=serviceImpl.addproductcategory(params);
			if(category!=0) {
				result=new JsonResult(Constants.STATUS_SUCCESS,"添加商品分类成功",category);
			}else {
				result=new JsonResult(Constants.STATUS_UNFOUND,"添加商品分类失败");	
			}
		}catch(Exception  e) {
			result=new JsonResult(Constants.STATUS_FAILURE,"添加异常",e.getMessage());
		}
		JsonResultWriter.writer(response, result);	
	}
}
