package edu.fashion.api.product;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import edu.fashion.common.Constants;
import edu.fashion.common.JsonResult;
import edu.fashion.common.JsonResultWriter;
import edu.fashion.model.PageModel;
import edu.fashion.model.Product;
import edu.fashion.service.product.SelectProductCategoryAscServiceImpl;

/**
 * 李政奇
 * 小类别价格升序查询
 * 参数 小类别
 * Servlet implementation class SelectProductCategoryAscServlet
 */
@WebServlet("/PriceCategoryAsc")
public class SelectProductCategoryAscServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String categoryName=request.getParameter("categoryName");
		String page=request.getParameter("page");
		String pageSize=request.getParameter("pageSize");
		JsonResult<PageModel<Product>> result=null;
		SelectProductCategoryAscServiceImpl psas=new SelectProductCategoryAscServiceImpl();
		try {
			PageModel<Product> pageModel=psas.selectCategoryProduct(categoryName, page, pageSize);
			if(pageModel!=null) {
				result=new JsonResult<PageModel<Product>> (Constants.STATUS_SUCCESS,"查询成功",pageModel);
			}else {
				result=new JsonResult<PageModel<Product>> (Constants.STATUS_UNFOUND,"没有数据");
			}
		} catch (Exception e) {
				result=new JsonResult<PageModel<Product>> (Constants.STATUS_FAILURE,"查询异常",e.getMessage());
		}
		JsonResultWriter.writer(response, result);
	}
}
