package edu.fashion.api.product;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import edu.fashion.common.Constants;
import edu.fashion.common.JsonResult;
import edu.fashion.common.JsonResultWriter;
import edu.fashion.model.PageModel;
import edu.fashion.model.Product;
import edu.fashion.service.product.SelectDateAllProductAscServiceImpl;
/**李政奇
 * 查询所有商品
 * 按照时间升序
 * Servlet implementation class SelectDateAllProductAscServlet
 */
@WebServlet("/Product/DateAllProductAsc")
public class SelectDateAllProductAscServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SelectDateAllProductAscServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String page=request.getParameter("page");
		String pageSize=request.getParameter("pageSize");
		JsonResult<PageModel<Product>> result=null;
		SelectDateAllProductAscServiceImpl psas=new SelectDateAllProductAscServiceImpl();
		try {
			PageModel<Product> products=psas.dateAllProductAsc(page, pageSize);
			if(products!=null) {
				result=new JsonResult<PageModel<Product>> (Constants.STATUS_SUCCESS,"查询成功",products);
			}else {
				result=new JsonResult<PageModel<Product>> (Constants.STATUS_UNFOUND,"没有数据");
			}
		} catch (Exception e) {
			result=new JsonResult<PageModel<Product>>(Constants.STATUS_FAILURE,"查询异常",e.getMessage());
		}
		JsonResultWriter.writer(response, result);
	}
}
