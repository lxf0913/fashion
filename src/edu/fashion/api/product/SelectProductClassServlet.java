package edu.fashion.api.product;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import edu.fashion.common.Constants;
import edu.fashion.common.JsonResult;
import edu.fashion.common.JsonResultWriter;
import edu.fashion.model.Product;
import edu.fashion.service.product.SelectProductClassServiceImpl;

//根据大类别查询商品信息  chaochao
//参数  classname
@WebServlet("/SelectProductClassServlet")
public class SelectProductClassServlet extends HttpServlet{
	private static final long serialVersionUID = 1L;
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String classname=request.getParameter("className");
		JsonResult<List<Product>> result=null;
		SelectProductClassServiceImpl spcsi=new SelectProductClassServiceImpl();
		try {
			List<Product> products=spcsi.selectproductclass(classname);
			if(products!=null) {
				result=new JsonResult<List<Product>> (Constants.STATUS_SUCCESS,"查询成功",products);
			}else {
				result=new JsonResult<List<Product>> (Constants.STATUS_UNFOUND,"查询失败");
			}
		} catch (Exception e) {
			result=new JsonResult<List<Product>> (Constants.STATUS_FAILURE,"查询异常",e.getMessage());
		}
		JsonResultWriter.writer(response, result);
	}


}
