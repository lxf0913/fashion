package edu.fashion.api.product;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import edu.fashion.common.Constants;
import edu.fashion.common.JsonResult;
import edu.fashion.common.JsonResultWriter;
import edu.fashion.model.PageModel;
import edu.fashion.model.Product;
import edu.fashion.service.product.SelectAllProductServiceImpl;

/**
 * 李政奇
 * 所有商品查询
 * 无需条件
 * Servlet implementation class SelectAllProductServlet
 */
@WebServlet("/AllProduct")
public class SelectAllProductServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SelectAllProductServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String page=request.getParameter("page");
		String pageSize=request.getParameter("pageSize");
		JsonResult<PageModel<Product>> result=null;
		SelectAllProductServiceImpl psas=new SelectAllProductServiceImpl();
		try {
			PageModel<Product> products=psas.selectAllProduct(page, pageSize);
			if(products!=null) {
				result=new JsonResult<PageModel<Product>> (Constants.STATUS_SUCCESS,"查询成功",products);
			}else {
				result=new JsonResult<PageModel<Product>> (Constants.STATUS_UNFOUND,"没有数据");
			}
		} catch (Exception e) {
			result=new JsonResult<PageModel<Product>>(Constants.STATUS_FAILURE,"查询异常",e.getMessage());
		}
		JsonResultWriter.writer(response, result);
	}
}
