package edu.fashion.api.product;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import edu.fashion.common.Constants;
import edu.fashion.common.JsonResult;
import edu.fashion.common.JsonResultWriter;
import edu.fashion.model.Product;
import edu.fashion.service.product.SelectSalesHotProductServiceImpl;

/**李政奇
 * 按照销量的无参数传递的热门商品查询显示四条
 * Servlet implementation class SelectSalsProductServlet
 */
@WebServlet("/hotProduct")
public class SelectSalsHotProductServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SelectSalsHotProductServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		JsonResult<List<Product>> result=null;
		SelectSalesHotProductServiceImpl hotProduct=new SelectSalesHotProductServiceImpl();
		try {
			List<Product> products=hotProduct.selectHotProduct();
			if(products!=null) {
				result=new JsonResult<List<Product>> (Constants.STATUS_SUCCESS,"查询成功",products);
			}else {
				result=new JsonResult<List<Product>> (Constants.STATUS_UNFOUND,"没有数据");
			}
		} catch (Exception e) {
			result=new JsonResult<List<Product>> (Constants.STATUS_FAILURE,"查询异常",e.getMessage());
		}
		JsonResultWriter.writer(response, result);
	}

}
