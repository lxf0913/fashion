package edu.fashion.api.product;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import edu.fashion.common.Constants;
import edu.fashion.common.JsonResult;
import edu.fashion.common.JsonResultWriter;
import edu.fashion.model.PageModel;
import edu.fashion.model.Product;
import edu.fashion.service.product.SelectSalesAllProductServiceImpl;

/**
 * 李政奇
 * 查询所有商品按销量排序
 * 降序
 * Servlet implementation class SelectProductAllServlet
 */
@WebServlet("/AllSales")
public class SelectSalesAllProductServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String page=request.getParameter("page");
		String pageSize=request.getParameter("pageSize");
		JsonResult<PageModel<Product>> result=null;
		SelectSalesAllProductServiceImpl psas=new SelectSalesAllProductServiceImpl();
		try {
			PageModel<Product> products=psas.selectAllProduct(page, pageSize);
			if(products!=null) {
				result=new JsonResult<PageModel<Product>> (Constants.STATUS_SUCCESS,"查询成功",products);
			}else {
				result=new JsonResult<PageModel<Product>> (Constants.STATUS_UNFOUND,"没有数据");
			}
		} catch (Exception e) {
			result=new JsonResult<PageModel<Product>>(Constants.STATUS_FAILURE,"查询异常",e.getMessage());
		}
		JsonResultWriter.writer(response, result);
	}
}
