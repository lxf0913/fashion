package edu.fashion.api.product;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import edu.fashion.common.Constants;
import edu.fashion.common.JsonResult;
import edu.fashion.common.JsonResultWriter;
import edu.fashion.dao.product.SelectCC;
/**
 * Servlet implementation class SelectCCServlet
 */
@WebServlet("/SelectCCServlet")
public class SelectCCServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		JsonResult<HashMap<String, List<String>>> result=null;
		SelectCC scc=new SelectCC();
		try {
			HashMap<String, List<String>> products=scc.selectClassC();
			if(products!=null) {
				result=new JsonResult<HashMap<String, List<String>>>(Constants.STATUS_SUCCESS,"查询成功",products);
			}else {
				result=new JsonResult<HashMap<String, List<String>>>(Constants.STATUS_UNFOUND,"没有数据");
			}
		} catch (Exception e) {
			result=new JsonResult<HashMap<String, List<String>>>(Constants.STATUS_FAILURE,"查询异常",e.getMessage());
		}
		JsonResultWriter.writer(response, result);
	
	
	}

}
