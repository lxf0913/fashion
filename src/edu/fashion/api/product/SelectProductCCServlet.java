package edu.fashion.api.product;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import edu.fashion.common.Constants;
import edu.fashion.common.JsonResult;
import edu.fashion.common.JsonResultWriter;
import edu.fashion.model.Product;
import edu.fashion.service.product.SelectProductCCService;
/**
 * lxf
 * 按大小类查询
 * 参数 大类  小类别
 * Servlet implementation class SelectProductCCServlet
 */
@WebServlet("/SelectProductCCServlet")
public class SelectProductCCServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String classname=request.getParameter("className");
		String category=request.getParameter("categoryName");
		JsonResult<List<Product>> result=null;
		SelectProductCCService psas=new SelectProductCCService();
		try {
			List<Product> products=psas.selectCategoryProduct(classname, category);
			if(products!=null) {
				result=new JsonResult<List<Product>> (Constants.STATUS_SUCCESS,"查询成功",products);
			}else {
				result=new JsonResult<List<Product>> (Constants.STATUS_UNFOUND,"没有数据");
			}
		} catch (Exception e) {
			result=new JsonResult<List<Product>> (Constants.STATUS_FAILURE,"查询异常",e.getMessage());
		}
		JsonResultWriter.writer(response, result);
	}

}
