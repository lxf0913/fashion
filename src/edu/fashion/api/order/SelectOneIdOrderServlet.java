package edu.fashion.api.order;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import edu.fashion.common.Constants;
import edu.fashion.common.JsonResult;
import edu.fashion.common.JsonResultWriter;
import edu.fashion.model.Order;
import edu.fashion.service.order.SelectOneIdOrderServiceImpl;

/**李政奇
 * 根据用户ID和订单ID查询详细订单
 * Servlet implementation class SelectOneIdOrderServlet
 */
@WebServlet("/OneIdOrder")
public class SelectOneIdOrderServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SelectOneIdOrderServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String orderUserId = request.getParameter("orderUserId");
		String orderId =request.getParameter("orderId");
		JsonResult<Order> result=null;
		try{
			SelectOneIdOrderServiceImpl orderOne=new SelectOneIdOrderServiceImpl();
			List<Order> orders  = orderOne.selectOneOrder(orderUserId, orderId);
			if(orders!=null){
			result=new JsonResult(Constants.STATUS_SUCCESS,"添加成功",orders);
		}else{
			result=new JsonResult(Constants.STATUS_UNFOUND,"添加失败");
		}}catch(Exception e){
			result=new JsonResult(Constants.STATUS_FAILURE,"添加异常",e.getMessage());
		}
			JsonResultWriter.writer(response, result);
	}	
}
