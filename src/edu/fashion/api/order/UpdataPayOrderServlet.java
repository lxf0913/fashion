package edu.fashion.api.order;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import edu.fashion.common.Constants;
import edu.fashion.common.JsonResult;
import edu.fashion.common.JsonResultWriter;
import edu.fashion.service.order.UpdataPayOrderServiceImpl;

/**根据订单ID
 * 设置用户的支付状态为已支付
 * 李政奇
 * Servlet implementation class UpdataPayOrderServlet
 */
@WebServlet("/UpdataPayOrder")
public class UpdataPayOrderServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdataPayOrderServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String orderId =request.getParameter("orderId");
		JsonResult<Integer> result=null;
		UpdataPayOrderServiceImpl updataPay =new UpdataPayOrderServiceImpl();
		try {
			int orderPay=updataPay.updataPay(orderId);
			if(orderPay>0) {
				result=new JsonResult<Integer> (Constants.STATUS_SUCCESS,"查询成功",orderPay);
			}else {
				result=new JsonResult<Integer> (Constants.STATUS_UNFOUND,"没有数据");
			}
		} catch (Exception e) {
			result=new JsonResult<Integer>(Constants.STATUS_FAILURE,"查询异常",e.getMessage());
		}
		JsonResultWriter.writer(response, result);
	}
}
