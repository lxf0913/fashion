package edu.fashion.api.order;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import edu.fashion.common.Constants;
import edu.fashion.common.JsonResult;
import edu.fashion.common.JsonResultWriter;
import edu.fashion.model.Order;
import edu.fashion.service.order.SelectOrderDetailServiceImpl;

/**李政奇
 * 查询订单详请
 * Servlet implementation class SelectOrderDetailServlet
 */
@WebServlet("/OrderDetail")
public class SelectOrderDetailServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SelectOrderDetailServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String orderUserId = request.getParameter("orderUserId");
		String orderId =request.getParameter("orderId");
		JsonResult<Order> result=null;
		try{
			SelectOrderDetailServiceImpl orderDetaile=new SelectOrderDetailServiceImpl();
			Order order  = orderDetaile.selectOrderDetailSI(orderUserId, orderId);
			if(order!=null){
			result=new JsonResult<Order>(Constants.STATUS_SUCCESS,"添加成功",order);
		}else{
			result=new JsonResult<Order>(Constants.STATUS_UNFOUND,"添加失败");
		}}catch(Exception e){
			result=new JsonResult<Order>(Constants.STATUS_FAILURE,"添加异常",e.getMessage());
		}
			JsonResultWriter.writer(response, result);
	}

}
