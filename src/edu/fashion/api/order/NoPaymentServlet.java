package edu.fashion.api.order;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import edu.fashion.common.Constants;
import edu.fashion.common.JsonResult;
import edu.fashion.common.JsonResultWriter;
import edu.fashion.model.Order;
import edu.fashion.model.PageModel;
import edu.fashion.service.order.NoPaymentService;
	@WebServlet("/NoPaymentServlet")
	public class NoPaymentServlet extends HttpServlet {
		private static final long serialVersionUID = 1L;
	       
	    /**
	     * @see HttpServlet#HttpServlet()
	     */
	    public NoPaymentServlet() {
	        super();
	        // TODO Auto-generated constructor stub
	    }

		/**
		 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
		 */
		protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			// TODO Auto-generated method stub
			String orderUserId =request.getParameter("orderUserId");
			NoPaymentService npms=new NoPaymentService();
			JsonResult result=null;
			int i=npms.nopay(orderUserId);
			try {
				
				if(i>=0) {
					result=new JsonResult(Constants.STATUS_SUCCESS,"查询成功",i);
				}else {
					result=new JsonResult(Constants.STATUS_UNFOUND,"没有数据");
				}
			} catch (Exception e) {
				result=new JsonResult(Constants.STATUS_FAILURE,"查询异常",e.getMessage());
			}
			JsonResultWriter.writer(response, result);
		}
	}
