package edu.fashion.api.collection;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import edu.fashion.common.Constants;
import edu.fashion.common.JsonResult;
import edu.fashion.common.JsonResultWriter;
import edu.fashion.service.collection.InsertCollectionServiceImpl;

/**
 * lxf
 * Servlet implementation class InsertCollection
 */
@WebServlet("/InsertCollection")
public class InsertCollection extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String userid=request.getParameter("userid");
		String productid=request.getParameter("productid");
		JsonResult<Integer> result=null;
		try{
			InsertCollectionServiceImpl urs=new InsertCollectionServiceImpl();
			int i=urs.insert(userid, productid);
			if(i!=0){
			result=new JsonResult<Integer>(Constants.STATUS_SUCCESS,"添加成功",i);
		}else{
			result=new JsonResult<Integer>(Constants.STATUS_UNFOUND,"添加失败");
		}}catch(Exception e){
			result=new JsonResult<Integer>(Constants.STATUS_FAILURE,"添加异常",e.getMessage());
		}
			JsonResultWriter.writer(response, result);
	}

}
