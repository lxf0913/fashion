package edu.fashion.api.collection;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import edu.fashion.common.Constants;
import edu.fashion.common.JsonResult;
import edu.fashion.common.JsonResultWriter;
import edu.fashion.service.collection.SelectCollectionService;
import edu.fashion.service.shopcar.SelectShopcarServicelmpl;

/**
 * Servlet implementation class SelectCollection
 */
@WebServlet("/SelectCollection")
public class SelectCollection extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String userid=request.getParameter("userid");
		JsonResult<List<HashMap<String, Object>>> result =null;

		SelectCollectionService selectallshop = new SelectCollectionService();
			try {
				List<HashMap<String, Object>> shops= selectallshop.selectservice(userid);
				if(shops.size()!=0){
					result=new JsonResult<List<HashMap<String, Object>>>(Constants.STATUS_SUCCESS,"查询成功",shops);
				}else {
					result=new JsonResult<List<HashMap<String, Object>>>(Constants.STATUS_UNFOUND,"用户没有收藏");
				}
			} catch (Exception e) {
				result =new JsonResult<List<HashMap<String, Object>>>(Constants.STATUS_FAILURE, "查询收藏商品异常", e.getMessage());
			}
			JsonResultWriter.writer(response, result);
	}

}
