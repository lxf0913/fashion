package edu.fashion.api.collection;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import edu.fashion.common.Constants;
import edu.fashion.common.JsonResult;
import edu.fashion.common.JsonResultWriter;
import edu.fashion.service.collection.DeleteCollectionService;

/**
 * lxf
 * Servlet implementation class DeleteCollection
 */
@WebServlet("/DeleteAllCollection")
public class DeleteAllCollection extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String userid=request.getParameter("userid");
		JsonResult<Integer> result=null;
		try{
			DeleteCollectionService urs=new DeleteCollectionService();
			int i=urs.deleteCollection(userid);
			if(i!=0){
			result=new JsonResult<Integer>(Constants.STATUS_SUCCESS,"ɾ���ɹ�",i);
		}else{
			result=new JsonResult<Integer>(Constants.STATUS_UNFOUND,"ɾ��ʧ��");
		}}catch(Exception e){
			result=new JsonResult<Integer>(Constants.STATUS_FAILURE,"ɾ���쳣",e.getMessage());
		}
			JsonResultWriter.writer(response, result);
	
	}

}
