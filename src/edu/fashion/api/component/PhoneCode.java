package edu.fashion.api.component;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import edu.fashion.common.Constants;
import edu.fashion.common.JsonResult;
import edu.fashion.common.JsonResultWriter;
import edu.fashion.common.PhoneCodeConfig;
import edu.fashion.util.HttpUtil;
import java.net.URLEncoder;
import java.util.Random;



/** 
 * Servlet implementation class IndustrySMSServlet
 */
@WebServlet("/PhoneCode")
public class PhoneCode extends HttpServlet {
	private static final long serialVersionUID = 1L;

    private static String operation = "/industrySMS/sendSMS";

    private static String accountSid = PhoneCodeConfig.ACCOUNT_SID;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public PhoneCode() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		   
	       String to=request.getParameter("phonenumber");
	       //创建一个JsonResult封装消息，然后转为json，变成一个对象{}
	       JsonResult result=null;
	       Random random = new Random();
	       String code="";
	       for (int i=0;i<6;i++)
	       {
	           code+=random.nextInt(10);
	       }
	       if(to==null||to=="") {
	           result=new JsonResult (Constants.STATUS_UNFOUND,"短信号码未匹配");
	       }else {
	           String tmpSmsContent = null;
	           
               String smsContent = "【Fashion时装】您的验证码为"+code+"，请于5分钟内正确输入，如非本人操作，请忽略此短信。";
	            try{
	              tmpSmsContent = URLEncoder.encode(smsContent, "UTF-8");
	            }catch(Exception e){
	              e.printStackTrace();
	            }
	            
	            String url = PhoneCodeConfig.BASE_URL + operation;
	            String body = "accountSid=" + accountSid + "&to=" + to + "&smsContent=" + tmpSmsContent
	                + HttpUtil.createCommonParam();

	            // 提交请求
	            String backresult = HttpUtil.post(url, body);
	            result=new JsonResult(Constants.STATUS_SUCCESS,"发送成功",code);
	            
	            System.out.println("result:" + System.lineSeparator() + backresult);
	       }
	     //将响应和结果  数据，转为json
	     JsonResultWriter.writer(response, result);  
	}

}
