package edu.fashion.api.user;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import edu.fashion.common.Constants;
import edu.fashion.common.JsonResult;
import edu.fashion.common.JsonResultWriter;
import edu.fashion.model.User;
import edu.fashion.service.user.UserDetailServiceImpl;
import edu.fashion.service.user.UserLoginServiceImpl;

/**
 * Servlet implementation class UserMyDetailServlet
 */
@WebServlet("/UserMyDetailServlet")
public class UserMyDetailServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String userid = request.getParameter("userid");
		UserDetailServiceImpl uds=new UserDetailServiceImpl();
		JsonResult<User> result =null;
		try {
			User user = uds.userdetail(userid);
			if(user!=null){
				result=new JsonResult<User>(Constants.STATUS_SUCCESS,"查询成功",user);
			}else {
				result=new JsonResult<User>(Constants.STATUS_UNFOUND,"查询失败");
			}
		} catch (Exception e) {
			result =new JsonResult<User>(Constants.STATUS_FAILURE, "查询异常", e.getMessage());
		}
		JsonResultWriter.writer(response, result);
	}

}
