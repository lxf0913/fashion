package edu.fashion.api.user;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import edu.fashion.common.Constants;
import edu.fashion.common.JsonResult;
import edu.fashion.common.JsonResultWriter;
import edu.fashion.service.user.DeleteUserServiceImpl;

/**李政奇
 * 根据用户名删除用户
 * Servlet implementation class DeleteIdUserServlet
 */
@WebServlet("/DeleteIdUser")
public class DeleteIdUserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DeleteIdUserServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String userId=request.getParameter("userId");
		JsonResult<Integer> result=null;
		DeleteUserServiceImpl deleteUser = new DeleteUserServiceImpl();
		try {
			int userDelete=deleteUser.deleteUser(userId);
			if(userDelete>0) {
				result=new JsonResult<Integer> (Constants.STATUS_SUCCESS,"查询成功",userDelete);
			}else {
				result=new JsonResult<Integer> (Constants.STATUS_UNFOUND,"没有数据");
			}
		} catch (Exception e) {
			result=new JsonResult<Integer>(Constants.STATUS_FAILURE,"查询异常",e.getMessage());
		}
		JsonResultWriter.writer(response, result);
	}
}
