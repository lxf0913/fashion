package edu.fashion.api.user;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import edu.fashion.common.Constants;
import edu.fashion.common.JsonResult;
import edu.fashion.common.JsonResultWriter;
import edu.fashion.service.user.UserUpdateServiceImpl;

/**
 * lxf
 * Servlet implementation class UserUpdateServlet
 */
@WebServlet("/UserUpdateServlet")
public class UserUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String useraddress = request.getParameter("useraddress");
		String useremail = request.getParameter("useremail");
		String usertelephone = request.getParameter("usertelephone");
		String usersex = request.getParameter("usersex");
		String userhead = request.getParameter("userhead");
		String userid = request.getParameter("userid");
		String username = request.getParameter("username");
		JsonResult<Integer> result=null;
		try{
			UserUpdateServiceImpl urs=new UserUpdateServiceImpl();
			int i=urs.update(useraddress, useremail, usertelephone, usersex, userhead, userid,username);
			if(i!=0){
			result=new JsonResult<Integer>(Constants.STATUS_SUCCESS,"修改成功",i);
		}else{
			result=new JsonResult<Integer>(Constants.STATUS_UNFOUND,"修改失败");
		}}catch(Exception e){
			result=new JsonResult<Integer>(Constants.STATUS_FAILURE,"修改异常",e.getMessage());
		}
			JsonResultWriter.writer(response, result);
	}

}
