package edu.fashion.api.user;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import edu.fashion.common.Constants;
import edu.fashion.common.JsonResult;
import edu.fashion.common.JsonResultWriter;
import edu.fashion.model.PageModel;
import edu.fashion.model.User;
import edu.fashion.service.user.SelectDateAllUserServiceImpl;

/**查询所有用户
 * 按时间降序
 * 李政奇
 * Servlet implementation class SelectDateAllUserServlet
 */
@WebServlet("/DateAllUser")
public class SelectDateAllUserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SelectDateAllUserServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String page=request.getParameter("page");
		JsonResult<PageModel<User>> result=null;
		SelectDateAllUserServiceImpl alluser=new SelectDateAllUserServiceImpl();
		try {
			PageModel<User> users=alluser.selectAllUserSI(page);
			if(users!=null) {
				result=new JsonResult<PageModel<User>> (Constants.STATUS_SUCCESS,"查询成功",users);
			}else {
				result=new JsonResult<PageModel<User>> (Constants.STATUS_UNFOUND,"没有数据");
			}
		} catch (Exception e) {
			result=new JsonResult<PageModel<User>>(Constants.STATUS_FAILURE,"查询异常",e.getMessage());
		}
		JsonResultWriter.writer(response, result);
	}
}
