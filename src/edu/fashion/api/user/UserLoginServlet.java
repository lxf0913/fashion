package edu.fashion.api.user;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import edu.fashion.common.Constants;
import edu.fashion.common.JsonResult;
import edu.fashion.common.JsonResultWriter;
import edu.fashion.model.User;
import edu.fashion.service.user.UserLoginServiceImpl;
/**
 * 
 * @author 奇奇
 * 用户登录功能
 */
@WebServlet("/UserLogin")
public class UserLoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String user_name = request.getParameter("user_name");
		String user_password = request.getParameter("user_password");
//		String yzm =request.getParameter("yzm");
		JsonResult result =null;
//		if(request.getSession().getAttribute()=yzm){
			UserLoginServiceImpl login = new UserLoginServiceImpl();
			try {
				User user = login.userLSI(user_name, user_password);
				if(user!=null){
					result=new JsonResult(Constants.STATUS_SUCCESS,"登陆成功",user);
				}else {
					result=new JsonResult(Constants.STATUS_UNFOUND,"密码错误或用户不存在");
				}
			} catch (Exception e) {
				// TODO: handle exception
				result =new JsonResult(Constants.STATUS_FAILURE, "登陆异常", e.getMessage());
			}
			JsonResultWriter.writer(response, result);
//		}
	}
}
