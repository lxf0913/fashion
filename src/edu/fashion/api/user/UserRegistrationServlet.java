package edu.fashion.api.user;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import edu.fashion.common.Constants;
import edu.fashion.common.JsonResult;
import edu.fashion.common.JsonResultWriter;
import edu.fashion.service.user.UserRegistrationServicelmp;
//肖赵阳
/**
 * Servlet implementation class UserRegistrationServlet
 */
@WebServlet("/UserRegistrationServlet")
public class UserRegistrationServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String useraccount=request.getParameter("useraccount");
		String userpassword=request.getParameter("userpassword");
		String username=request.getParameter("username");
		String useremail=request.getParameter("useremail");
		String usertelephone=request.getParameter("usertelephone");
		String usersex=request.getParameter("usersex");
		String userhead=request.getParameter("userhead");
		JsonResult result=null;
		try{
			UserRegistrationServicelmp urs=new UserRegistrationServicelmp();
			int i=urs.userregistrationserver(useraccount, userpassword, username, useremail, usertelephone, usersex, userhead,"无");
			if(i!=0){
			result=new JsonResult(Constants.STATUS_SUCCESS,"注册成功",i);
		}else{
			result=new JsonResult(Constants.STATUS_UNFOUND,"已有用户名注册");
		}}catch(Exception e){
			result=new JsonResult(Constants.STATUS_FAILURE,"注册异常",e.getMessage());
		}
			JsonResultWriter.writer(response, result);
	}

}
