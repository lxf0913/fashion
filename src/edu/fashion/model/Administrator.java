package edu.fashion.model;

public class Administrator {
	private String administratorId;
	private String administratorPasswrod;
	private String administratorName;
	public String getAdministratorId() {
		return administratorId;
	}
	public void setAdministratorId(String administratorId) {
		this.administratorId = administratorId;
	}
	public String getAdministratorPasswrod() {
		return administratorPasswrod;
	}
	public void setAdministratorPasswrod(String administratorPasswrod) {
		this.administratorPasswrod = administratorPasswrod;
	}
	public String getAdministratorName() {
		return administratorName;
	}
	public void setAdministratorName(String administratorName) {
		this.administratorName = administratorName;
	}
}
