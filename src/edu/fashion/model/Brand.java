package edu.fashion.model;

public class Brand {
	private String brandId;
	private String brandName;
	private String brandStatus;
	private String brandProductId;
	public String getBrandId() {
		return brandId;
	}
	public void setBrandId(String brandId) {
		this.brandId = brandId;
	}
	public String getBrandName() {
		return brandName;
	}
	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}
	public String getBrandStatus() {
		return brandStatus;
	}
	public void setBrandStatus(String brandStatus) {
		this.brandStatus = brandStatus;
	}
	public String getBrandProductId() {
		return brandProductId;
	}
	public void setBrandProductId(String brandProductId) {
		this.brandProductId = brandProductId;
	}
}
