package edu.fashion.model;

public class User {
		private String userId;
		private String userAccount;
		private String userPasswrod;
		private String userName;
		private String userEmail;
		private String userTelephone;
		private String userSex;
		private String userHead;
		private String userTime;
		private String address;
		public String getAddress() {
			return address;
		}
		public void setAddress(String address) {
			this.address = address;
		}
		public String getUserId() {
			return userId;
		}
		public void setUserId(String userId) {
			this.userId = userId;
		}
		public String getUserAccount() {
			return userAccount;
		}
		public void setUserAccount(String userAccount) {
			this.userAccount = userAccount;
		}
		public String getUserPasswrod() {
			return userPasswrod;
		}
		public void setUserPasswrod(String userPasswrod) {
			this.userPasswrod = userPasswrod;
		}
		public String getUserName() {
			return userName;
		}
		public void setUserName(String userName) {
			this.userName = userName;
		}
		public String getUserEmail() {
			return userEmail;
		}
		public void setUserEmail(String userEmail) {
			this.userEmail = userEmail;
		}
		public String getUserTelephone() {
			return userTelephone;
		}
		public void setUserTelephone(String userTelephone) {
			this.userTelephone = userTelephone;
		}
		public String getUserSex() {
			return userSex;
		}
		public void setUserSex(String userSex) {
			this.userSex = userSex;
		}
		public String getUserHead() {
			return userHead;
		}
		public void setUserHead(String userHead) {
			this.userHead = userHead;
		}
		public String getUserTime() {
			return userTime;
		}
		public void setUserTime(String userTime) {
			this.userTime = userTime;
		}	
}
