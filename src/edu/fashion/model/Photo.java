package edu.fashion.model;

public class Photo {
	private String photoId;
	private String photoName;
	private String photoProductId;
	public String getPhotoId() {
		return photoId;
	}
	public void setPhotoId(String photoId) {
		this.photoId = photoId;
	}
	public String getPhotoName() {
		return photoName;
	}
	public void setPhotoName(String photoName) {
		this.photoName = photoName;
	}
	public String getPhotoProductId() {
		return photoProductId;
	}
	public void setPhotoProductId(String photoProductId) {
		this.photoProductId = photoProductId;
	}
}
