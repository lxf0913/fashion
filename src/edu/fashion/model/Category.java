package edu.fashion.model;

public class Category {
	private String categoryId;
	private String categoryName;
	private String categoryFather;
	private String categoryStatus;
	private String categoryProductId;
	public String getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public String getCategoryFather() {
		return categoryFather;
	}
	public void setCategoryFather(String categoryFather) {
		this.categoryFather = categoryFather;
	}
	public String getCategoryStatus() {
		return categoryStatus;
	}
	public void setCategoryStatus(String categoryStatus) {
		this.categoryStatus = categoryStatus;
	}
	public String getCategoryProductId() {
		return categoryProductId;
	}
	public void setCategoryProductId(String categoryProductId) {
		this.categoryProductId = categoryProductId;
	}
}
