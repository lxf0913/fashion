package edu.fashion.model;
  
public class Address {
	private String addressId;
	private String addressLocation;
	private String addressUserId;
	private String addressLevel;
	public String getAddressId() {
		return addressId;
	}
	public void setAddressId(String addressId) {
		this.addressId = addressId;
	}
	public String getAddressLocation() {
		return addressLocation;
	}
	public void setAddressLocation(String addressLocation) {
		this.addressLocation = addressLocation;
	}
	public String getAddressUserId() {
		return addressUserId;
	}
	public void setAddressUserId(String addressUserId) {
		this.addressUserId = addressUserId;
	}
	public String getAddressLevel() {
		return addressLevel;
	}
	public void setAddressLevel(String addressLevel) {
		this.addressLevel = addressLevel;
	}
}
