package edu.fashion.model;


public class ShopCar {
	private String shopcarId;
	private String shopcarProductId;
	private String shopcarUserId;
	private String shopcarOrderId;
	private String shopcarDate;
	private String shopcarStatus;
	private String shopcarNumber;
	private String shopprice;
	private String shopPrice;
	private String shopComment;
	private Product shopproduct;
	private String shopColor;
	private String shopSize;
	private String productname;
	private String productphoto;
	/**
	 * @return the shopprice
	 */
	public String getShopprice() {
		return shopprice;
	}
	/**
	 * @param shopprice the shopprice to set
	 */
	public void setShopprice(String shopprice) {
		this.shopprice = shopprice;
	}

	/**
	 * @return the productphoto
	 */
	public String getProductphoto() {
		return productphoto;
	}
	/**
	 * @param productphoto the productphoto to set
	 */
	public void setProductphoto(String productphoto) {
		this.productphoto = productphoto;
	}
	public String getProductname() {
		return productname;
	}
	public void setProductname(String productname) {
		this.productname = productname;
	}
	public String getShopColor() {
		return shopColor;
	}
	public void setShopColor(String shopColor) {
		this.shopColor = shopColor;
	}
	public String getShopSize() {
		return shopSize;
	}
	public void setShopSize(String shopSize) {
		this.shopSize = shopSize;
	}
	public Product getShopproduct() {
		return shopproduct;
	}
	public void setShopproduct(Product shopproduct) {
		this.shopproduct = shopproduct;
	}
	public String getShopcarId() {
		return shopcarId;
	}
	public void setShopcarId(String shopcarId) {
		this.shopcarId = shopcarId;
	}
	public String getShopcarProductId() {
		return shopcarProductId;
	}
	public void setShopcarProductId(String shopcarProductId) {
		this.shopcarProductId = shopcarProductId;
	}
	public String getShopcarUserId() {
		return shopcarUserId;
	}
	public void setShopcarUserId(String shopcarUserId) {
		this.shopcarUserId = shopcarUserId;
	}
	public String getShopcarOrderId() {
		return shopcarOrderId;
	}
	public void setShopcarOrderId(String shopcarOrderId) {
		this.shopcarOrderId = shopcarOrderId;
	}
	public String getShopcarDate() {
		return shopcarDate;
	}
	public void setShopcarDate(String shopcarDate) {
		this.shopcarDate = shopcarDate;
	}
	public String getShopcarStatus() {
		return shopcarStatus;
	}
	public void setShopcarStatus(String shopcarStatus) {
		this.shopcarStatus = shopcarStatus;
	}
	public String getShopcarNumber() {
		return shopcarNumber;
	}
	public void setShopcarNumber(String shopcarNumber) {
		this.shopcarNumber = shopcarNumber;
	}
	public String getShopPrice() {
		return shopPrice;
	}
	public void setShopPrice(String shopPrice) {
		this.shopPrice = shopPrice;
	}
	public String getShopComment() {
		return shopComment;
	}
	public void setShopComment(String shopComment) {
		this.shopComment = shopComment;
	}
}
