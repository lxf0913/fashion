package edu.fashion.util;

import java.util.ArrayList;

public class StringUtil1 {
	/**
	 * 从ajax JSON序列化的参数中获得原始数组
	 * @return
	 */
	public static ArrayList getArray(String str) {
	    ArrayList<String> arrs=new ArrayList<>();
	    String [] arrayStr = str.split(",");
	    for(String s : arrayStr) {
	        arrs.add(s);
	    }
	    return arrs;
	}

}
