package edu.fashion.util;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

public class MySqlUtil {
	private static Properties properties=new Properties();
	private static ThreadLocal<Connection> local=new ThreadLocal<Connection>();
	static{
		try {
			properties.load(MySqlUtil.class.getClassLoader().getResourceAsStream("jdbc.properties"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	static{
		try {
			Class.forName(properties.getProperty("driver"));
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("����ʧ��",e);
		}
	}
	public static Connection openConnection(){
		Connection conn=local.get();
		try {
			if(conn==null||conn.isClosed()){
				conn=DriverManager.getConnection(properties.getProperty("url"), properties.getProperty("user"), properties.getProperty("passwrold"));
				local.set(conn);
			}	
		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException("����ʧ��",e);
		}
		return conn;
	}
	
	public static List<HashMap<String, Object>> selectSql(String sql,Object ...obj){
		ResultSet rs=null;
		Connection conn=openConnection();
		List<HashMap<String, Object>> rows=new ArrayList<HashMap<String, Object>>();
		PreparedStatement pst=null;
		try {
			pst=conn.prepareStatement(sql);
			if(obj.length!=0){
				for(int i=0;i<obj.length;i++){
					pst.setObject(i+1, obj[i]);
				}
			}
			rs=pst.executeQuery();
			ResultSetMetaData rsm=rs.getMetaData();
			int length=rsm.getColumnCount();
			while(rs.next()){
				HashMap<String, Object> map=new HashMap<String, Object>();
				for(int i=0;i<length;i++){
					String s=rsm.getColumnLabel(i+1);
					map.put(s, rs.getObject(s));
				}
				rows.add(map);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException("��ѯʧ��",e);
		}finally{
			if(rs!=null){
				try {
					rs.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if(pst!=null){
				try {
					pst.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return rows;
	}
	public static void close(){
		Connection con = local.get();
		if(con!=null){
			try {
				con.close();
				local.remove();
				con=null;
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	public static int updateSql(String sql,Object ...obj){
		int i1=0;
		Connection conn=openConnection();
		PreparedStatement pst=null;
		try {
			pst=conn.prepareStatement(sql);
			if(obj.length!=0){
				for(int i=0;i<obj.length;i++){
					pst.setObject(i+1, obj[i]);
				}
			}
			i1=pst.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException(e.getMessage(),e);
		}finally{
			if(pst!=null){
				try {
					pst.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return i1;
	}	
}