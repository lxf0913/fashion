package edu.fashion.service.background;

import edu.fashion.dao.background.SelectLikeUserNameOrderDaoImpl;
import edu.fashion.model.Order;
import edu.fashion.model.PageModel;
/**
 * 模糊查询订单  分页  时间降序
 * @author 李政奇
 *
 */
public class SelectLikeUserNameOrderServiceImpl {
	public PageModel<Order> like(String username,String page,String pageSize) {
		String userNameNull=username.replaceAll("\\s*", "");
		SelectLikeUserNameOrderDaoImpl likeUser=new SelectLikeUserNameOrderDaoImpl();
		return likeUser.like(userNameNull, page, pageSize);
	}
}
