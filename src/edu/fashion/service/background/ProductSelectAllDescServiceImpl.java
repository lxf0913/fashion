package edu.fashion.service.background;


import edu.fashion.dao.background.SelectProductAllDescDaoImpl;
import edu.fashion.model.PageModel;
import edu.fashion.model.Product;
/*
 * 李政奇 按价钱降序查询
 */
public class ProductSelectAllDescServiceImpl {
	public PageModel<Product> selectAllDescProduct(String page,String pageSize) {
		SelectProductAllDescDaoImpl spad=new SelectProductAllDescDaoImpl();
		return spad.selectPADIdesc(page, pageSize);
	}
}
