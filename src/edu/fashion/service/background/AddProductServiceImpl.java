package edu.fashion.service.background;

import edu.fashion.dao.background.SelectCategoryOneDaoImpl;
import edu.fashion.dao.background.SelectClassOneDaoImpl;
import edu.fashion.dao.background.AddProductDaoImpl;
import edu.fashion.model.Category;
import edu.fashion.model.Classes;

/**
 * 添加商品  注意 产品的大类别和小类别的Id是在Service上插入的
 * @author 李政奇
 *
 */
public class AddProductServiceImpl {
	public int addProduct(String productNmae,String productPrice,String categoryName,String productCount,String className,String productPhoto) {
		SelectClassOneDaoImpl addClass =new SelectClassOneDaoImpl();
		Classes classes=addClass.addClass(className);
		String classId =classes.getClassId();
		SelectCategoryOneDaoImpl addCategory =new SelectCategoryOneDaoImpl();
		Category category=addCategory.addCategory(categoryName);
		String categoryId=category.getCategoryId();
		AddProductDaoImpl add=new AddProductDaoImpl();
		int i=add.addProduct(productNmae, productPrice, categoryId, productCount, classId, productPhoto);
		return i;
	}
}
