package edu.fashion.service.background;


import edu.fashion.dao.background.SelectProductAllAscDaoImpl;
import edu.fashion.model.PageModel;
import edu.fashion.model.Product;
/*
 * 李政奇
 * 商品价格降序
 */
public class ProductSelectAllAscServiceImpl {
	public PageModel<Product> selectAllAscProduct(String page,String pageSize) {
		SelectProductAllAscDaoImpl spad=new SelectProductAllAscDaoImpl();
		return spad.selectPADIasc(page, pageSize);
	}
}
