package edu.fashion.service.background;
/**
 * 按照Id删除特定商品
 * @author 李政奇
 *
 */

import edu.fashion.dao.background.DelectIdProductDaoImpl;

public class DelectIdProductServiceImpl {
	public int delectId(String productId) {
		DelectIdProductDaoImpl delectId=new DelectIdProductDaoImpl();
		return delectId.delectProduct(productId);
	}
}
