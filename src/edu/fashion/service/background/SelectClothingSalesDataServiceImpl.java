package edu.fashion.service.background;

import java.util.List;

import edu.fashion.dao.background.SelectClothingSalesDateDaoImpl;

/**
 * 返回小类别名 按销量排序 与 后面小类别的销量相对应
 * @author 李政奇
 *
 */
public class SelectClothingSalesDataServiceImpl {
	public List<List<Object>> clothingSales(){
		SelectClothingSalesDateDaoImpl clothingSales=new SelectClothingSalesDateDaoImpl();
		return clothingSales.selectCategoryDI();
	}
}
