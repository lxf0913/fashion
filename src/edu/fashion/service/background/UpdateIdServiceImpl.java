package edu.fashion.service.background;

import edu.fashion.dao.background.SelectCategoryOneDaoImpl;
import edu.fashion.dao.background.SelectClassOneDaoImpl;
import edu.fashion.dao.background.UpdateIdProductDaoImpl;
import edu.fashion.model.Category;
import edu.fashion.model.Classes;

/**
 * 
 * @author李政奇 
 * 	根据用户Id修改商品信息 
 *
 */
public class UpdateIdServiceImpl {
	public int upDateId(String productName,String productPrice,String productStatus,String productPhoto,String productId,String productclass,String productcategory,String count) {
		UpdateIdProductDaoImpl update =new UpdateIdProductDaoImpl();
		SelectClassOneDaoImpl addClass =new SelectClassOneDaoImpl();
		Classes classes=addClass.addClass(productclass);
		String classId =classes.getClassId();
		SelectCategoryOneDaoImpl addCategory =new SelectCategoryOneDaoImpl();
		Category category=addCategory.addCategory(productcategory);
		String categoryId=category.getCategoryId();
		return update.updateProduct(productName, productPrice, productStatus, productPhoto, productId,classId,categoryId,count);
	}
}
