package edu.fashion.service.background;

import edu.fashion.dao.background.UPdateDeliverDaoImpl;

/**
 * 根据订单号修改是否发货
 * @author 李政奇
 *
 */
public class UPdateDeliverServiceImpl {
	public int updateDeliverSI(String orderId) {
		UPdateDeliverDaoImpl update =new UPdateDeliverDaoImpl();
		return update.updateDeliverDI(orderId);
	}
}
