package edu.fashion.service.background;

import edu.fashion.dao.background.SelectProductByNameDaoImpl;
import edu.fashion.model.PageModel;
import edu.fashion.model.Product;

public class SelectProductByNameServiceImpl {
	SelectProductByNameDaoImpl spbn=new SelectProductByNameDaoImpl();
	public PageModel<Product> selectproductbyname(String productname,String page,String pageSize) {
		String productName=productname.replaceAll("\\s*", "");
		return spbn.selectpbn(productName, page, pageSize);
	}
}
