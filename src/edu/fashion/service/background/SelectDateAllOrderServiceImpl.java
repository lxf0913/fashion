package edu.fashion.service.background;


import edu.fashion.dao.background.SelectDateAllOrderDaoImpl;
import edu.fashion.model.Order;
import edu.fashion.model.PageModel;


/**
 * 查询订单所有 按时间降序
 * @author 47796
 *
 */

public class SelectDateAllOrderServiceImpl {
	public PageModel<Order> selectDateAllOrderSI(String page,String pageSize) {
		SelectDateAllOrderDaoImpl dateAllOrder =new SelectDateAllOrderDaoImpl();
		return dateAllOrder.dateAllOrder(page, pageSize);
	}
}
