package edu.fashion.service.collection;

import java.util.HashMap;
import java.util.List;
import edu.fashion.dao.collection.SelectColectionDao;
import edu.fashion.dao.product.SelectProductDaoImpl;

public class SelectCollectionService {
	SelectColectionDao shopdao = new SelectColectionDao();
	SelectProductDaoImpl pdap = new SelectProductDaoImpl();

	public List<HashMap<String, Object>> selectservice(String userid) {
		List<HashMap<String, Object>> ss = shopdao.select(userid);
		for (int i = 0; i < ss.size(); i++) {
			ss.get(i).put("product", pdap.selectpdi(ss.get(i).get("productid").toString()));
		}
		return ss;
	}
}
