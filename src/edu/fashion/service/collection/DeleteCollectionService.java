package edu.fashion.service.collection;

import edu.fashion.dao.collection.DeleteCollectionDao;

public class DeleteCollectionService {
	DeleteCollectionDao dcd=new DeleteCollectionDao();
	public int deleteCollection(String userid) {
		return dcd.deletecollection(userid);
	}
}
