package edu.fashion.service.collection;

import edu.fashion.dao.collection.DeletedCollectDao;

public class DeletedCollectionService {
	DeletedCollectDao dcd=new DeletedCollectDao();
	public int deleteCollection(String collectionid) {
		return dcd.deletedcollection(collectionid);
	}
}
