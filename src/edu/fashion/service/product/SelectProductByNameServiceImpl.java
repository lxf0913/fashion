package edu.fashion.service.product;

import edu.fashion.dao.product.SelectProductByNameDaoImpl;
import edu.fashion.model.PageModel;
import edu.fashion.model.Product;
/**
 * ģ��������Ʒ
 * @author 47796
 *
 */
public class SelectProductByNameServiceImpl {
	public PageModel<Product> selectproductbyname(String productname,String page,String pageSize) {
		SelectProductByNameDaoImpl spbn=new SelectProductByNameDaoImpl();
		String productnameNull=productname.replaceAll("\\s*", "");
		return  spbn.selectpbn(productnameNull, page, pageSize);
	}
}
