package edu.fashion.service.product;

import edu.fashion.dao.product.SelectSalesCategoryDaoLmpl;
import edu.fashion.model.PageModel;
import edu.fashion.model.Product;

/*小类别 销量降序 查询
 * 李政奇
 */
public class SelectSalesCategoryServiceImpl {
	public PageModel<Product> selectCategoryProduct(String categoryName,String page,String pageSize) {
		SelectSalesCategoryDaoLmpl spad=new SelectSalesCategoryDaoLmpl();
		return spad.selectCategory(categoryName, page, pageSize);
	}
}
