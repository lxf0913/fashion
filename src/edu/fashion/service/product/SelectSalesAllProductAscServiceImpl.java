package edu.fashion.service.product;

import edu.fashion.dao.product.SelectSalesAllProductAscDaoImpl;
import edu.fashion.model.PageModel;
import edu.fashion.model.Product;

/**
 * 按销量 升序
 * 查询所有商品
 * @author 李政奇
 *
 */
public class SelectSalesAllProductAscServiceImpl {
	public PageModel<Product> selectAllProduct(String page,String pageSize) {
		SelectSalesAllProductAscDaoImpl spad=new SelectSalesAllProductAscDaoImpl();
		return spad.salesAllProductAsc(page, pageSize);
	}
}
