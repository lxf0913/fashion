package edu.fashion.service.product;


import edu.fashion.dao.product.SelectDateCategoryDaoImpl;
import edu.fashion.model.PageModel;
import edu.fashion.model.Product;

/**
 * 小类别 时间降序排序
 * @author 李政奇
 *
 */
public class SelectDateCategoryServiceImpl {
	public PageModel<Product> selectDateCategory(String categoryName,String page,String pageSize) {
		SelectDateCategoryDaoImpl dateCategory=new SelectDateCategoryDaoImpl();
		return dateCategory.selectCategory(categoryName, page, pageSize);
	}
}
