package edu.fashion.service.product;

import java.util.List;
import edu.fashion.dao.product.SelectSalesHotProductDaoImpl;
import edu.fashion.model.Product;
/**
 * 热门商品查询 销量 四条数据
 * @author 李政奇
 *
 */
public class SelectSalesHotProductServiceImpl {
	SelectSalesHotProductDaoImpl hotProduct=new SelectSalesHotProductDaoImpl();
	public List<Product> selectHotProduct() {
		return hotProduct.selectPADI();
	}
}
