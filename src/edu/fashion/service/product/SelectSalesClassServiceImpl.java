package edu.fashion.service.product;

import edu.fashion.dao.product.SelectSalesClassDaoImpl;
import edu.fashion.model.PageModel;
import edu.fashion.model.Product;
/*
 *大类别查询  降序排序
 *李政奇
 */
public class SelectSalesClassServiceImpl {
	SelectSalesClassDaoImpl spad=new SelectSalesClassDaoImpl();
	public PageModel<Product> selectClassProduct(String className,String page,String pageSize) {
		return spad.selectSalesClass(className, page, pageSize);
	}
}
