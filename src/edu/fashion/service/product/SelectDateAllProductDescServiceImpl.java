package edu.fashion.service.product;

import edu.fashion.dao.product.SelectDateAllProductDescDaoImpl;
import edu.fashion.model.PageModel;
import edu.fashion.model.Product;

/**
 * 时间查询所有商品
 * 降序
 * @author 李政奇
 *
 */
public class SelectDateAllProductDescServiceImpl {
	public PageModel<Product> dateAllProductDesc(String page,String pageSize) {
		SelectDateAllProductDescDaoImpl spad=new SelectDateAllProductDescDaoImpl();
		return spad.dateAllProductDesc(page, pageSize);
	}
}
