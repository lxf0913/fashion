package edu.fashion.service.product;

import java.util.List;

import edu.fashion.dao.product.SelectProductClassDaoImpl;
import edu.fashion.model.Product;

public class SelectProductClassServiceImpl {
	SelectProductClassDaoImpl spcdi=new SelectProductClassDaoImpl();
	public List<Product> selectproductclass(String className) {
		return spcdi.selectproductclass(className);
	}
}
