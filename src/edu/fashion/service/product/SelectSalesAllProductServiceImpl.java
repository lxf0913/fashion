package edu.fashion.service.product;

import edu.fashion.dao.product.SelectSalesAllProductDaoImpl;
import edu.fashion.model.PageModel;
import edu.fashion.model.Product;
/*所有商品 销量排序降序
 * 李政奇
 */
public class SelectSalesAllProductServiceImpl {
	public PageModel<Product> selectAllProduct(String page,String pageSize) {
		SelectSalesAllProductDaoImpl spad=new SelectSalesAllProductDaoImpl();
		return spad.selectPADI(page, pageSize);
	}
}
