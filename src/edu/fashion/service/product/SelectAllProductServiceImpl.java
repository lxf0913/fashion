package edu.fashion.service.product;
/**
 * 查询所有商品
 * 无需排序
 */
import edu.fashion.dao.product.SelectProductAll;
import edu.fashion.model.PageModel;
import edu.fashion.model.Product;

public class SelectAllProductServiceImpl {
	public PageModel<Product> selectAllProduct(String page,String pageSize) {
		SelectProductAll spad=new SelectProductAll();
		return spad.selectAllProduct(page, pageSize);
	}
}
