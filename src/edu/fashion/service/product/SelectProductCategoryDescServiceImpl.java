package edu.fashion.service.product;

import edu.fashion.dao.product.SelectCategoryDescDaoLmpl;
import edu.fashion.model.PageModel;
import edu.fashion.model.Product;
/*
 * 李政奇 
 * 价格查询所有 
 * 降序
 */
public class SelectProductCategoryDescServiceImpl {
	public PageModel<Product> selectCategoryProduct(String categoryName,String page,String pageSize) {
		SelectCategoryDescDaoLmpl spad=new SelectCategoryDescDaoLmpl();
		return spad.selectCategoryDesc(categoryName, page, pageSize);
	}
}
