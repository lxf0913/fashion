package edu.fashion.service.product;

import edu.fashion.dao.product.SelectDateAllProductAscDaoImpl;
import edu.fashion.model.PageModel;
import edu.fashion.model.Product;

/**
 * 
 * @author 李政奇
 *  
 * 查询所有商品 
 * 按照时间升序
 */
public class SelectDateAllProductAscServiceImpl {
	public PageModel<Product> dateAllProductAsc(String page,String pageSize) {
		SelectDateAllProductAscDaoImpl spad=new SelectDateAllProductAscDaoImpl();
		return spad.dateAllProductAsc(page, pageSize);
	}
}
