package edu.fashion.service.user;

import edu.fashion.dao.user.SelectAllUserDaoImpl;
import edu.fashion.model.PageModel;
import edu.fashion.model.User;

public class SelectAllUserServiceImpl {
	public PageModel<User> selectAllUserSI(String page){
		SelectAllUserDaoImpl allUser=new SelectAllUserDaoImpl();
		return allUser.selectAllUser(page);
	}
}
