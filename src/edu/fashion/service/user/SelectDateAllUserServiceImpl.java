package edu.fashion.service.user;

import edu.fashion.dao.user.SelectDateUserAllDaoImpl;
import edu.fashion.model.PageModel;
import edu.fashion.model.User;

public class SelectDateAllUserServiceImpl {
	public PageModel<User> selectAllUserSI(String page){
		SelectDateUserAllDaoImpl allUser=new SelectDateUserAllDaoImpl();
		return allUser.selectDateAllUser(page);
	}
}
