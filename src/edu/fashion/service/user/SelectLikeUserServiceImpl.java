package edu.fashion.service.user;

import edu.fashion.dao.user.SelectLikeUserDaoImpl;
import edu.fashion.model.PageModel;
import edu.fashion.model.User;
/*
 * 李政奇
 * 模糊搜索用户名
 */
public class SelectLikeUserServiceImpl {
	public PageModel<User> selectAllUserSI(String page,String userName){
		SelectLikeUserDaoImpl allUser=new SelectLikeUserDaoImpl();
		String userNameNull=userName.replaceAll("\\s*", "");
		return allUser.selectLikeUser(page, userNameNull);
	}
}
 