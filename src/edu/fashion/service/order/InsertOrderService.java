package edu.fashion.service.order;

import edu.fashion.common.RandomOrderId;
import edu.fashion.dao.order.InsertOrderDaoImpl;
import edu.fashion.dao.shopcar.Update;

public class InsertOrderService {
	InsertOrderDaoImpl iod=new InsertOrderDaoImpl();
	Update u=new Update();
	public int insertOrder(String userid,String address,String shopid,String price) {
		String orderid=RandomOrderId.getGuid();
		int i=iod.insertOrder(orderid, userid, address, price);
		String [] arr=shopid.split(",");
		for(int k=0;k<arr.length;k++) {
			u.update(arr[k], orderid);
		}
		return i;
	}

}
