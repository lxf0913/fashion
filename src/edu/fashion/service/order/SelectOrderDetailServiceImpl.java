package edu.fashion.service.order;

import java.util.List;

import edu.fashion.dao.order.SelectOrderDetailDaoImpl;
import edu.fashion.dao.product.SelectOrderIdProductDetailDaoImpl;
import edu.fashion.dao.user.SelectUserOneIdDaoImpl;
import edu.fashion.model.Order;
import edu.fashion.model.Product;
import edu.fashion.model.User;

public class SelectOrderDetailServiceImpl {
	public Order selectOrderDetailSI(String orderUserId,String orderId) {
		SelectOrderDetailDaoImpl orderDetail=new SelectOrderDetailDaoImpl();
		Order order=orderDetail.orderDatail(orderUserId, orderId);
		SelectUserOneIdDaoImpl selectUser =new SelectUserOneIdDaoImpl();
		User user=selectUser.userOne(orderUserId);
		order.setOrderUser(user);
		SelectOrderIdProductDetailDaoImpl productDI =new SelectOrderIdProductDetailDaoImpl();
		List<Product> products = productDI.orderIdProductList(orderId);
		order.setProducts(products);
		return order;
	}
}
