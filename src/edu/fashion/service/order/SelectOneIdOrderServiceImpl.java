package edu.fashion.service.order;

import java.util.List;

import edu.fashion.dao.order.SelectOneIdOrderDaoImpl;
import edu.fashion.model.Order;

public class SelectOneIdOrderServiceImpl {
	public List<Order> selectOneOrder(String orderUserId,String orderId) {
		SelectOneIdOrderDaoImpl orderone=new SelectOneIdOrderDaoImpl();
		return orderone.selectOneOrder(orderUserId, orderId);
	}
}
