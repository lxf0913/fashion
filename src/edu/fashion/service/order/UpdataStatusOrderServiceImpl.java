package edu.fashion.service.order;

import edu.fashion.dao.order.UpdataStatusOrderDaoImpl;

/**
 * 根据用户Id
 * 改变用户的状态为1
 * @author 李政奇
 *
 */
public class UpdataStatusOrderServiceImpl {
	public  int updataStatus(String orderId) {
		UpdataStatusOrderDaoImpl updata =new UpdataStatusOrderDaoImpl();
		return updata.statusOrder(orderId);
	}
}
