package edu.fashion.service.order;

import edu.fashion.dao.order.SelectOrderDaoImpl;
import edu.fashion.model.Order;
import edu.fashion.model.PageModel;
/**
 * 用户订单查询 按user_id 
 * @author 李政奇
 *
 */
public class SelectOrderServiceImpl {
	public PageModel<Order> selectSI(String orderUserId,String page){
		SelectOrderDaoImpl order=new SelectOrderDaoImpl();
		return order.selectODI(orderUserId, page);
	}
}
