package com.alipay;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import edu.fashion.service.order.UpdataPayOrderServiceImpl;

/**
 * Servlet implementation class NotifyurlServlet
 */
@WebServlet("/api/NotifyurlServlet")
public class NotifyurlServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//		Map<String,String> params = new HashMap<String,String>();
//		Map<String,String[]> requestParams = request.getParameterMap();
//		for (Iterator<String> iter = requestParams.keySet().iterator(); iter.hasNext();) {
//			String name = (String) iter.next();
//			String[] values = (String[]) requestParams.get(name);
//			String valueStr = "";
//			for (int i = 0; i < values.length; i++) {
//				valueStr = (i == values.length - 1) ? valueStr + values[i]
//						: valueStr + values[i] + ",";
//			}
//			//乱码解决，这段代码在出现乱码时使用
//			valueStr = new String(valueStr.getBytes("ISO-8859-1"), "utf-8");
//			params.put(name, valueStr);
//		}
//		boolean signVerified=true;
//		try {
//			signVerified = AlipaySignature.rsaCheckV1(params, "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAvDRpgWkECZnZufPSfQDRgyc4ZeDuL8rH8g9+u6TgJB+bWI4VXdHlIl23ow8AzVDnd4zr5irx7PYwjCxOxGCwRe2Yl5I81jh1br+/78DYU1p6SD8CYBsvWjFgGwa26YZ2AVQpdTV2LtMSLaFc/Ak61GeOA+uu02+HdpJEljqlY/THhfNao3WC+hGS2APY1PCFUdDzRfH1egy6m1kDHaDEkBaeOv4vdDjpZni+b5i3lAXfLTRXsOVSTm4Qxv572/sidfB4tu8Uem+vNtYIp8mWgE5AVxce4lbNI/fDn5oMlUHxd6cBtIU8PmkwEINTeIT4fBIugmL8RMViFxRMNwwfpQIDAQAB", "utf-8", "RSA2");
//		} catch (AlipayApiException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} //调用SDK验证签名
//		if(signVerified) {//验证成功
			//商户订单号
			String out_trade_no = new String(request.getParameter("out_trade_no").getBytes("ISO-8859-1"),"UTF-8");
			String trade_status = new String(request.getParameter("trade_status").getBytes("ISO-8859-1"),"UTF-8");
			if(trade_status.equals("TRADE_SUCCESS")) {
				UpdataPayOrderServiceImpl updataPay =new UpdataPayOrderServiceImpl();
				updataPay.updataPay(out_trade_no);
				response.getWriter().println("success");
			}
			//支付宝交易号
//			String trade_no = new String(request.getParameter("trade_no").getBytes("ISO-8859-1"),"UTF-8");
		
			
		System.out.println(out_trade_no);
		System.out.println(trade_status);	
//			response.getWriter().println("success");
			
//		}else {//验证失败
//			response.getWriter().println("fail");
		
			//调试用，写文本函数记录程序运行情况是否正常
			//String sWord = AlipaySignature.getSignCheckContentV1(params);
			//AlipayConfig.logResult(sWord);
//		}
	}

}
