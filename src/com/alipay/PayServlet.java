package com.alipay;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.request.AlipayTradePagePayRequest;

/**
 * Servlet implementation class PayServlet
 */
@WebServlet("/api/PayServlet")
public class PayServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		AlipayClient alipayClient = new DefaultAlipayClient ("https://openapi.alipaydev.com/gateway.do","2016091700535311","MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQC8NGmBaQQJmdm589J9ANGDJzhl4O4vysfyD367pOAkH5tYjhVd0eUiXbejDwDNUOd3jOvmKvHs9jCMLE7EYLBF7ZiXkjzWOHVuv7/vwNhTWnpIPwJgGy9aMWAbBrbphnYBVCl1NXYu0xItoVz8CTrUZ44D667Tb4d2kkSWOqVj9MeF81qjdYL6EZLYA9jU8IVR0PNF8fV6DLqbWQMdoMSQFp46/i90OOlmeL5vmLeUBd8tNFew5VJObhDG/nvb+yJ18Hi27xR6b6821ginyZaATkBXFx7iVs0j98OfmgyVQfF3pwG0hTw+aTAQg1N4hPh8Ei6CYvxExWIXFEw3DB+lAgMBAAECggEBAKmX0Tr8FtdP/BLE0/gwtJsqzHeBqSEH6gUGUX1lpVTn5mR2JGrWqVO+f6w/6MkbDnl8U1alfDu73SNzCceMGUvqPobXEFs5pRiXUa4KaOUzst1HaiyQUqtDswanlkv5Dn1ECdfExSqMqOTVc6Tp7Elax25hArkk1zzRsVxunHrkmjiUEaNXQW2ZWKwxNbdHXXLIXmXY8RbUiGq5t8bqlXXH2A2BxRTZM1Hl9sq+Za4JMmqsPPTaMQ8Vu5lGIanoc/CBoS7n9KCWvY6HW7U21GR8M1oQZ3MfN6iDcLHi1SvqVXOF5o3FeDX0vHcu/Pm4FMzewePUhXlhLuXzANQpnEECgYEA92lBLirakwcEirCNZXHxks9BZhkblYiz+F/T4+PCqPV4A2zsU/jIQ2NbbfKlKPyCA24S8o7si9EwTxQpOv1sbL4l4rFik97dQLb9PUPBWF4t/B8MyY26CSxKcNhZ1qGhRlBq4MDof3ckWU2V7LnlsfGxvwnns08yEsFFk/opOZsCgYEAwrz9UDpIhPp+rqiQc6CZLcRbLl7hyewwmpAgnI2Shf9WwmKXf7Fobci+iKo4OC/39ie6XZFmIIt/4LSyvDACVrpnHpXGDdpKp30e3vbr1WxWH6PgKDuLll2nhtfP48Sa5+uJsk9SFRdAdXhRZbxhe+y19Yc6HhruM8TpHZunP78CgYEAjThnHBMZ8BJxZTihfOYtHb9vdPef5xrAn2UTbWD0G70BY/MockXvCYgNCBUYcOywyYRrDEjIfIZLLNg1MJa27NschOj7e6waXnYU1oN2ORqI5QBULDfZmKLPWtojbX+Urq34U5HJI/i9prvCd/0kPw3Wh1UADDmIO2xWasDAB0ECgYAu45qKyr3TZw1BuMyvSqgXYQQjci0jlX9OeA2iJGR3ZzfYriyUTn86j+9wsKvAu6wU1RQGm4/fnjc4P8oZjeXCRDe4IZACneED2xinzwSR6vgBvk+m4E+H8Rz5VtVznk8+MHSDPp4g/exDXKkcufMvB5v4dosD3LAmPu9ydZwbuQKBgDpKTlb5KB35o5Q+rz1JHJ7fAX9jSk5nCx3RJlyJKbhStC62Fj9H5IUnQIjoXbL1OvwfU1+7+Rtev1VX1KnGX8LGW2DZ18g935pb5eDi9FArCIZqUms1+LVsI37sZ6mziGM2IOK0wdl3aL2876NGnL0eXxxIQKTbdOkRqkWbeKZU","json","utf-8",
				"MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA6fVoAvdIHjmvh2iU2enP8WsmgaU1LXlt9+BmRTqJAZVv4lLKdufcfeOZj20FRI7GWlRZvr9dQAizwASgvVP7KnOYZ8KeU/ZviLy9l07SHoix+Br2UPgCbrvveF+EDeB8zXoreFukA81JGP1XGl7s1auzgEAmXOjxFIr+DlVNQH392K9GwFkZrTmwpUmeZcXTBOUYVe4uzpivzg9bu3qtztR14pbQi9RCiFGULUSjlhUb1VSsXqODMq1NTMWI7z3wMznUMIonalnpxpuIhKyQ6cKms77hNErwl5yW9pr6AFXe/YnSCUaU28FylkcM94/G33UlgZHolnkk5nmYVHOMewIDAQAB","RSA2" );
		AlipayTradePagePayRequest alipayRequest = new AlipayTradePagePayRequest();//创建API对应的request
	    alipayRequest.setReturnUrl("http://39.105.13.36/FashionWeb/MyOrder.html");
	    alipayRequest.setNotifyUrl("http://39.105.13.36/fashion/api/NotifyurlServlet");//在公共参数中设置回跳和通知地址
	    String re=request.getParameter("orderid");
	    String out_trade_no =re.split(",")[0];
		String total_amount =re.split(",")[1];
	    alipayRequest.setBizContent("{\"out_trade_no\":\""+ out_trade_no +"\"," 
				+ "\"total_amount\":\""+ total_amount +"\"," 
				+ "\"subject\":\""+ "Fashion商城" +"\"," 
				+ "\"body\":\""+ "lll" +"\"," 
				+ "\"product_code\":\"FAST_INSTANT_TRADE_PAY\"}");
	    String form="";
	    try {
	        form = alipayClient.pageExecute(alipayRequest).getBody(); //调用SDK生成表单
	    } catch (AlipayApiException e) {
	        e.printStackTrace();
	    }
	    response.setContentType("text/html;charset=" + "utf-8");
	    response.getWriter().write(form);//直接将完整的表单html输出到页面
	    response.getWriter().flush();
	    response.getWriter().close();
	}
		
	

}
